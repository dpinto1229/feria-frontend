## Descripción

Proyecto para la reserva de stands con NextJS/React.

## Installación de dependencias

```bash
$ npm install
```
## Configuración del proyecto
```bash
Paso 1: Copiar el archivo .env.example a .env

cp .env.example .env

```
## Ejecución del proyecto

```bash
# modo producción
$ npm run start

# modo desarrollo
$ npm run dev

# modo desarrollo
$ npm run start:dev
```
