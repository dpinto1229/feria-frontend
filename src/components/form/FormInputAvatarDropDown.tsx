import React from 'react';
import {
    Control,
    Controller,
    FieldValues,
    Path,
    PathValue,
  } from 'react-hook-form';
  import {
    Avatar,
    FormHelperText,
    InputLabel,
    MenuItem,
    Select,
    SelectChangeEvent,
    Typography,
  } from '@mui/material';
  import { RegisterOptions } from 'react-hook-form/dist/types/validator';
  import { Variant } from '@mui/material/styles/createTypography';
  
  export interface OptionType {
    id: number;
    nombre: string;
    avatarSrc: string;
  }
  
  type FormInputAvatarDropdownProps<T extends FieldValues> = {
    id: string;
    name: Path<T>;
    control: Control<T, object>;
    label: string;
    size?: 'small' | 'medium';
    rules?: RegisterOptions;
    disabled?: boolean;
    onChange?: (event: SelectChangeEvent) => void;
    clearable?: boolean;
    bgcolor?: string;
    // options: OptionType[];
    options: string[];
    labelVariant?: Variant;
  };
  
  export const FormInputAvatarDropdown = <T extends FieldValues>({
    id,
    name,
    control,
    label,
    size = 'medium',
    rules,
    disabled,
    onChange,
    options,
    clearable,
    bgcolor,
    labelVariant = 'subtitle2',
  }: FormInputAvatarDropdownProps<T>) => {
    const generateSelectOptions = () =>
      options.map((option) => (
        <MenuItem key={option} value={option}>
          <Avatar src={option} sx={{ width: 100, height: 100 }} />
        </MenuItem>
      ));
  
    return (
      <div>
        <InputLabel htmlFor={id}>
          <Typography
            variant={labelVariant}
            sx={{
              pb: 1,
              fontWeight: 'fontWeightMedium',
              color: 'text.primary',
            }}
          >
            {label}
          </Typography>
        </InputLabel>
        <Controller
          name={name}
          control={control}
          render={({ field, fieldState: { error } }) => (
            <>
              <Select
                id={id}
                name={name}
                sx={{
                  display: 'flex',
                  flexWrap: 'wrap', // Para que los elementos estén en una fila horizontal

                  bgcolor: bgcolor,
                  '& .MuiSelect-iconOutlined': {
                    display: field.value && clearable ? 'none' : '',
                  },
                  '&.Mui-focused .MuiIconButton-root': {
                    color: 'primary.main',
                  },
                }}
                size={size}
                error={!!error}
                disabled={disabled}
                onChange={(event) => {
                  if (onChange) {
                    onChange(event);
                  }
                  field.onChange(event);
                }}
                inputRef={field.ref}
                value={field.value}
                IconComponent={() => null} // Para ocultar el ícono del select
              >
                {generateSelectOptions()}
              </Select>
              {!!error && (
                <FormHelperText error>{error?.message}</FormHelperText>
              )}
            </>
          )}
          defaultValue={'' as PathValue<T, Path<T>>}
          rules={rules}
        />
      </div>
    );
  };
  