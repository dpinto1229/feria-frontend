import React, { useEffect, useState } from 'react'
import {
  Avatar,
  Box,
  Button,
  Collapse,
  Divider,
  Drawer,
  Grid,
  List,
  ListItemButton,
  Typography,
  useMediaQuery,
  useTheme,
} from '@mui/material'
import { usePathname, useRouter } from 'next/navigation'
import Toolbar from '@mui/material/Toolbar'
import { imprimir } from '@/utils/imprimir'
import { titleCase } from '@/utils'
import { ModuloType } from '@/app/login/types/loginTypes'
import { useSidebar } from '@/context/SideBarProvider'
import { useAuth } from '@/context/AuthProvider'
import { Icono } from '@/components/Icono'
import { useFullScreenLoading } from '@/context/FullScreenLoadingProvider'
import { AlertDialog } from '../modales/AlertDialog'
import { useSession } from '@/hooks'

const drawerWidth = 220

type SidebarModuloType = ModuloType & { showed?: boolean; open?: boolean }

export const Sidebar = () => {
  const { sideMenuOpen, closeSideMenu, openSideMenu } = useSidebar()
  const { usuario, rolUsuario, estaAutenticado, progresoLogin } = useAuth()
  const [modulos, setModulos] = useState<SidebarModuloType[]>([])
  const [mostrarAlertaCerrarSesion, setMostrarAlertaCerrarSesion] =
    useState(false)
  const { cerrarSesion } = useSession()
  const theme = useTheme()
  const router = useRouter()
  const sm = useMediaQuery(theme.breakpoints.only('sm'))
  const md = useMediaQuery(theme.breakpoints.only('md'))
  const xs = useMediaQuery(theme.breakpoints.only('xs'))
  const { estadoFullScreen } = useFullScreenLoading()
  const pathname = usePathname()

  const getColorBasedOnTheme = () => {
    return theme.palette.mode === 'dark' ? '#333130' : '#404E67'
  }

  const interpretarModulos = () => {
    imprimir(`Cambio en módulos`)

    const rolSeleccionado = usuario?.roles.find(
      (itemRol) => itemRol.idRol == rolUsuario?.idRol
    )
    imprimir(`rolSeleccionado`, rolSeleccionado)

    setModulos(
      rolSeleccionado?.modulos.map((modulo) => ({ ...modulo, open: true })) ??
        []
    )
  }

  const rutaActiva = (routeName: string, currentRoute: string) =>
    currentRoute.includes(routeName, 0)

  const navigateTo = async (url: string) => {
    if (sm || xs || md) {
      closeSideMenu()
    }
    router.push(url)
  }

  useEffect(() => {
    if (sm || xs || md) {
      closeSideMenu()
    } else {
      openSideMenu()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sm, xs, md])

  const cerrarMenuSesion = async () => {
    await cerrarSesion()
  }
  useEffect(() => {
    interpretarModulos()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(usuario)])

  return (
    <>
      <AlertDialog
        isOpen={mostrarAlertaCerrarSesion}
        titulo={'Alerta'}
        texto={`¿Está seguro de cerrar sesión?`}
      >
        <Button
          onClick={() => {
            setMostrarAlertaCerrarSesion(false)
          }}
        >
          Cancelar
        </Button>
        <Button
          sx={{ fontWeight: 'medium' }}
          onClick={async () => {
            setMostrarAlertaCerrarSesion(false)
            await cerrarMenuSesion()
          }}
        >
          Aceptar
        </Button>
      </AlertDialog>
      <Drawer
        variant={sm || xs || md ? 'temporary' : 'persistent'}
        open={
          sideMenuOpen &&
          estaAutenticado &&
          modulos.length > 0 &&
          !progresoLogin &&
          !estadoFullScreen
        }
        onClose={closeSideMenu}
        ModalProps={{
          keepMounted: true,
        }}
        sx={{
          width: sideMenuOpen ? drawerWidth : `0`,
          border: 'none',
          flexShrink: 0,
          [`& .MuiDrawer-paper`]: {
            width: drawerWidth,
            borderWidth: 0.0,
            boxSizing: 'border-box',
            backgroundColor: getColorBasedOnTheme(),
            color: '#fff',
          },
          transition: 'all 0.2s ease-out',
        }}
      >
        <Toolbar />
        <Grid
          container
          direction={'column'}
          justifyContent={'center'}
          alignContent={'center'}
          pt={1}
        >
          {usuario?.avatar === null ? (
            <Avatar
              sx={{
                width: 110,
                height: 110,
                marginRight: 2,
              }}
            >
              {`${usuario?.persona?.nombres?.charAt(0) || ''}${
                usuario?.persona?.primerApellido?.charAt(0) || ''
              }`.toUpperCase()}
            </Avatar>
          ) : (
            <Avatar
              alt="Avatar"
              src={usuario?.avatar}
              sx={{ width: '150px', height: '150px' }}
            />
          )}
          <Box height={'10px'} />
          <Typography variant={'body2'}>
            {titleCase(usuario?.persona?.nombres ?? '')}{' '}
            {titleCase(
              usuario?.persona?.primerApellido ??
                usuario?.persona?.segundoApellido ??
                ''
            )}
          </Typography>
        </Grid>
        <Box
          sx={{
            overflow: 'auto',
            '&::-webkit-scrollbar': {
              width: '8px',
            },
            '&::-webkit-scrollbar-track': {
              background: '#f1f1f1',
            },
            '&::-webkit-scrollbar-thumb': {
              background: '#888',
            },
            '&::-webkit-scrollbar-thumb:hover': {
              background: '#555',
            },
          }}
        >
          {modulos.map((modulo, index) => (
            <div key={`div-${index}`}>
              <Box
                sx={{
                  display: 'flex',
                  m: 0,
                  alignItems: 'center',
                  cursor: 'pointer',
                }}
                onClick={() => {
                  const tempModulos = [...modulos]
                  tempModulos[index].open = !tempModulos[index].open
                  setModulos(tempModulos)
                }}
                onMouseOver={() => {
                  const tempModulos = [...modulos]
                  tempModulos[index].showed = true
                  setModulos(tempModulos)
                }}
                onMouseLeave={() => {
                  const tempModulos = [...modulos]
                  tempModulos[index].showed = false
                  setModulos(tempModulos)
                }}
              >
                <Box
                  sx={{
                    display: 'flex',
                    flexDirection: 'row',
                    m: 0,
                    borderRadius: 1,
                    alignItems: 'center',
                    margin: '16px 6px',
                    width: '100%',
                  }}
                >
                  <Box width={'20px'} />
                  <Typography
                    variant={'body2'}
                    color={'white'}
                  >{`${modulo.label}`}</Typography>
                  <Box sx={{ flexGrow: 1 }} />
                  {(modulo.showed || !modulo.open) && (
                    <Icono
                      fontSize={'small'}
                      sx={{ p: 0, m: 0, mx: 1.5 }}
                      color={'secondary'}
                    >
                      {modulo.open ? 'expand_more' : 'navigate_next'}
                    </Icono>
                  )}
                </Box>
              </Box>

              <Collapse in={modulo.open}>
                <List
                  key={`submodulos-${index}`}
                  component="ul"
                  style={{ cursor: 'pointer' }}
                  sx={{ pt: 0, pb: 0 }}
                >
                  {modulo.subModulo.map((subModuloItem, indexSubModulo) => (
                    <ListItemButton
                      id={`submodulo-${index}-${indexSubModulo}`}
                      key={`submodulo-${index}-${indexSubModulo}`}
                      component="li"
                      selected={rutaActiva(subModuloItem.url, pathname)}
                      onClick={() => navigateTo(subModuloItem.url)}
                      sx={{
                        display: 'flex',
                        flexDirection: 'row',
                        m: 0,
                        alignItems: 'center',
                        '&.Mui-selected': {
                          borderRadius: '15px',
                          backgroundColor:
                            theme.palette.mode === 'dark'
                              ? '#404E67'
                              : '#FE8A7D',
                        },
                      }}
                    >
                      <Box
                        sx={{
                          display: 'flex',
                          flexDirection: 'row',
                          m: 0,
                          borderRadius: 1,
                          alignItems: 'center',
                        }}
                      >
                        <Box width={'10px'} />
                        <Icono sx={{ color: 'white' }}>
                          {subModuloItem.propiedades.icono}
                        </Icono>
                        <Box width={'16px'} />
                        <Typography
                          variant={'body2'}
                        >{`${subModuloItem.label}`}</Typography>
                      </Box>
                    </ListItemButton>
                  ))}
                </List>
              </Collapse>
              {!modulo.open && <Divider sx={{ mx: 1 }} />}
            </div>
          ))}
        </Box>
      </Drawer>
    </>
  )
}
