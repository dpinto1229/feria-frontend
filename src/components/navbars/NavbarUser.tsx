'use client'
import {
  AppBar,
  Box,
  Button,
  DialogActions,
  DialogContent,
  Divider,
  FormControlLabel,
  Grid,
  IconButton,
  List,
  ListItem,
  Menu,
  MenuItem,
  Radio,
  Toolbar,
  Typography,
  useMediaQuery,
  useTheme,
} from '@mui/material'

import React, { useEffect, useState } from 'react'
import ThemeSwitcherButton from '../botones/ThemeSwitcherButton'

import {
  decodeBase64,
  delay,
  encodeBase64,
  InterpreteMensajes,
  siteName,
} from '@/utils'
import { useRouter } from 'next/navigation'

import { AlertDialog } from '../modales/AlertDialog'
import { imprimir } from '@/utils/imprimir'

import { useAlerts, useSession } from '@/hooks'
import { alpha } from '@mui/material/styles'
import { RoleType } from '@/app/login/types/loginTypes'
import { useAuth } from '@/context/AuthProvider'
import { useFullScreenLoading } from '@/context/FullScreenLoadingProvider'
import { useThemeContext } from '@/themes/ThemeRegistry'
import { Icono } from '@/components/Icono'
import { useSidebar } from '@/context/SideBarProvider'
import Image from 'next/image'
import { Constantes } from '@/config/Constantes'
import { CustomDialog } from '../modales/CustomDialog'
import ProgresoLineal from '../progreso/ProgresoLineal'
import { FormInputText } from '../form'
import { useForm } from 'react-hook-form'
import { TextService } from './login'

export const NavbarUser = () => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)

  const [roles, setRoles] = useState<RoleType[]>([])

  const { cerrarSesion, sesionPeticion } = useSession()

  const { usuario, setRolUsuario, rolUsuario } = useAuth()

  const { sideMenuOpen, closeSideMenu, openSideMenu } = useSidebar()

  const { mostrarFullScreen, ocultarFullScreen } = useFullScreenLoading()

  const [mostrarAlertaCerrarSesion, setMostrarAlertaCerrarSesion] =
    useState(false)
  const [abrirModalContrasena, setAbrirModalContrasena] = useState(false)

  const [idRolActual, setIdRolActual] = useState<string | undefined>()

  const abrirModal = (rolId: string) => {
    setIdRolActual(rolId)
    setAbrirModalContrasena(true)
  }

  const { control, handleSubmit, reset } = useForm({
    defaultValues: {
      contrasena: '',
    },
  })
  const [loadingModal, setLoadingModal] = useState(false)
  const cerrarModal = () => {
    setAbrirModalContrasena(false)
  }
  const router = useRouter()

  const { themeMode } = useThemeContext()

  const { Alerta } = useAlerts()
  const cambiarRol = async (event: React.ChangeEvent<HTMLInputElement>) => {
    imprimir(`Valor al hacer el cambio: ${event.target.value}`)
    cerrarMenu()
    mostrarFullScreen(`Cambiando de rol..`)
    await delay(1000)
    router.replace('/admin/home')
    await setRolUsuario({ idRol: `${event.target.value}` })
    ocultarFullScreen()
  }

  const desplegarMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const cerrarMenu = () => {
    setAnchorEl(null)
  }

  const cerrarMenuSesion = async () => {
    cerrarMenu()
    await cerrarSesion()
  }

  const interpretarRoles = () => {
    imprimir(`Cambio en roles 📜`, usuario?.roles)
    if (usuario?.roles && usuario?.roles.length > 0) {
      setRoles(usuario?.roles)
    }
  }

  /// Interpretando roles desde estado
  useEffect(() => {
    interpretarRoles()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [usuario])

  const theme = useTheme()
  // const sm = useMediaQuery(theme.breakpoints.only('sm'))
  const xs = useMediaQuery(theme.breakpoints.only('xs'))

  const accionMostrarAlertaCerrarSesion = () => {
    cerrarMenu()
    setMostrarAlertaCerrarSesion(true)
  }

  const compararContrasena = async (data: any) => {
    setLoadingModal(true)
    try {
      await delay(1000)
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/usuarios/validar-contrasena`,
        tipo: 'post',
        body: {
          contrasena: encodeBase64(encodeURI(data.contrasena)),
        },
      })
      if (respuesta.datos === true) {
        cambiarRol({ target: { value: idRolActual } } as any)
      }
    } catch (error) {
      imprimir(`Error al comparar contraseñas`, error)
      Alerta({ mensaje: `${InterpreteMensajes(error)}`, variant: 'error' })
    } finally {
      setLoadingModal(false)
      cerrarModal()
      reset()
    }
  }
  return (
    <>
      <AlertDialog
        isOpen={mostrarAlertaCerrarSesion}
        titulo={'Alerta'}
        texto={`¿Está seguro de cerrar sesión?`}
      >
        <Button
          onClick={() => {
            setMostrarAlertaCerrarSesion(false)
          }}
        >
          Cancelar
        </Button>
        <Button
          sx={{ fontWeight: 'medium' }}
          onClick={async () => {
            setMostrarAlertaCerrarSesion(false)
            await cerrarMenuSesion()
          }}
        >
          Aceptar
        </Button>
      </AlertDialog>
      <CustomDialog
        isOpen={abrirModalContrasena}
        handleClose={cerrarModal}
        title="Ingresar contraseña"
      >
        <DialogContent dividers>
          <Grid container direction={'column'} justifyContent="space-evenly">
            <Box height={'20px'} />
            <Grid container direction="row" spacing={{ xs: 2, sm: 1, md: 2 }}>
              <Grid item xs={12} sm={12} md={12}>
                <FormInputText
                  id={'contrasena'}
                  control={control}
                  name="contrasena"
                  label="Contraseña"
                  type={'password'}
                  disabled={loadingModal}
                  rules={{ required: 'Este campo es requerido' }}
                />
              </Grid>
            </Grid>
            <Grid></Grid>
            <Box height={'20px'} />
            <ProgresoLineal mostrar={loadingModal} />
          </Grid>
        </DialogContent>
        <DialogActions
          sx={{
            my: 1,
            mx: 2,
            justifyContent: {
              lg: 'flex-end',
              md: 'flex-end',
              xs: 'center',
              sm: 'center',
            },
          }}
        >
          <Button
            variant={'outlined'}
            disabled={loadingModal}
            onClick={cerrarModal}
          >
            Cancelar
          </Button>
          <Button
            variant={'contained'}
            disabled={loadingModal}
            onClick={handleSubmit(compararContrasena)}
          >
            Aceptar
          </Button>
        </DialogActions>
      </CustomDialog>
      <AppBar
        position="fixed"
        sx={{
          zIndex: (theme) => theme.zIndex.drawer + 1,
          backgroundColor: alpha(theme.palette.background.paper, 0.8),
          backdropFilter: 'blur(12px)',
        }}
      >
        <Toolbar>
          <IconButton
            id={'menu-sidebar'}
            size="large"
            aria-label="Menu lateral"
            name={sideMenuOpen ? 'Cerrar menú lateral' : 'Abrir menú lateral'}
            edge="start"
            color={'primary'}
            onClick={() => {
              if (sideMenuOpen) {
                closeSideMenu()
              } else {
                openSideMenu()
              }
            }}
            sx={{ mr: 1 }}
          >
            {sideMenuOpen ? (
              <Icono color={'primary'}>menu_open</Icono>
            ) : (
              <Icono color={'primary'}>menu</Icono>
            )}
          </IconButton>
          <Grid
            container
            alignItems={'center'}
            flexDirection={'row'}
            sx={{ flexGrow: 1 }}
          >
            <Box display={'inline-flex'}>
              <Grid
                container
                alignItems={'center'}
                flexDirection={'row'}
                justifyContent={'flex-start'}
                onClick={async () => {
                  await router.push('/admin/home')
                }}
                sx={{ cursor: 'pointer' }}
              >
                <Image
                  src={`${
                    themeMode === 'light'
                      ? `${Constantes.sitePath}/feria/logo_morado.png`
                      : `${Constantes.sitePath}/feria/logo_blanco.png`
                  }`}
                  alt="logo"
                  width={40}
                  height={40}
                />
                <Box sx={{ px: 0.5 }} />
                <Typography
                  color={'text.primary'}
                  component="div"
                  sx={{ fontWeight: '600' }}
                >
                  {siteName()}
                </Typography>
              </Grid>
            </Box>
          </Grid>
          {!xs && <ThemeSwitcherButton />}
          <Button size="small" onClick={desplegarMenu} color="primary">
            <Icono color={'primary'}>manage_accounts</Icono>
          </Button>
          <Menu
            id="menu-appbar"
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'right',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            open={Boolean(anchorEl)}
            onClose={cerrarMenu}
            autoFocus={false}
          >
            {roles.length > 1 && (
              <Box>
                <MenuItem
                  sx={{
                    p: 2,
                    ml: 0,
                    '&.MuiButtonBase-root:hover': {
                      bgcolor: 'transparent',
                    },
                  }}
                >
                  <Icono>switch_account</Icono>
                  <Box width={'20px'} />
                  <Typography variant={'body2'}>Roles </Typography>
                </MenuItem>
                <List key={`roles`} sx={{ p: 0 }}>
                  {roles.map((rol, indexRol) => (
                    <ListItem key={`rol-${indexRol}`}>
                      <Box
                        sx={{
                          display: 'flex',
                          flexDirection: 'row',
                          borderRadius: 1,
                          alignItems: 'center',
                        }}
                      >
                        <Box width={'20px'} />
                        <FormControlLabel
                          value={rol.idRol}
                          control={
                            <Radio
                              checked={rolUsuario?.idRol == rol.idRol}
                              onChange={
                                () => {
                                  abrirModal(rol.idRol)
                                }
                              }
                              color={'success'}
                              size="small"
                              value={rol.idRol}
                              name="radio-buttons"
                            />
                          }
                          componentsProps={{ typography: { variant: 'body2' } }}
                          label={rol.nombre}
                        />
                      </Box>
                    </ListItem>
                  ))}
                  <Divider />
                  <MenuItem
                    sx={{ px: 2.5, py: 1.5, mt: 1 }}
                    onClick={accionMostrarAlertaCerrarSesion}
                  >
                    <Icono color={'error'} fontSize={'small'}>
                      logout
                    </Icono>
                    <Box width={'15px'} />
                    <Typography
                      variant={'body2'}
                      fontWeight={'600'}
                      color={'error'}
                    >
                      Cerrar sesión
                    </Typography>
                  </MenuItem>
                </List>
              </Box>
            )}
          </Menu>
        </Toolbar>
      </AppBar>
    </>
  )
}
