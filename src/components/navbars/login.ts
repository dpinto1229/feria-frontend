import { hash } from 'bcrypt'

// Parámetros de configuración
export const Configurations = {
  SCORE_PASSWORD: 3, // Nivel mínimo de calificación del password
  SALT_ROUNDS: 10, // Número de saltos para generación de hash
  WRONG_LOGIN_LIMIT: 3, // Número máximo de intentos de inicio de sesión erróneos
  MINUTES_LOGIN_LOCK: 15, // Tiempo en minutos de bloqueo de cuenta
  // Lista de dominios de email no permitidos
  BLACK_LIST_EMAILS: [
    '10minutemail.com',
    'fremont.nodebalancer.linode.com',
    'yopmail.com',
    'cool.fr.nf',
    'jetable.fr.nf',
    'nospam.ze.tc',
    'nomail.xl.cx',
    'mega.zik.dj',
    'speed.1s.fr',
    'courriel.fr.nf',
    'moncourrier.fr.nf',
    'monemail.fr.nf',
    'monmail.fr.nf',
    'mailinator',
    'binkmail.com',
    'bobmail.info',
    'chammy.info',
    'devnullmail.com',
    'letthemeatspam.com',
    'mailinater.com',
    'mailinator.net',
    'mailinator2.com',
    'notmailinator.com',
    'reallymymail.com',
    'reconmail.com',
    'safetymail.info',
    'sendspamhere.com',
    'sogetthis.com',
    'spambooger.com',
    'spamherelots.com',
    'spamhereplease.com',
    'spamthisplease.com',
    'streetwisemail.com',
    'suremail.info',
    'thisisnotmyrealemail.com',
    'tradermail.info',
    'veryrealemail.com',
    'zippymail.info',
    'guerrillamail',
    'maildrop',
    'mailnesia',
  ],
}

export const TextService = {
  async encrypt(password: string) {
    const hashedPassword = await hash(password, Configurations.SALT_ROUNDS)
    return hashedPassword
  },
}
