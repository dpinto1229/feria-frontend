import { ReactNode, Suspense } from 'react'
import ThemeRegistry from '@/themes/ThemeRegistry'
import { NavbarLogin } from '@/components/navbars/NavbarLogin'
import Box from '@mui/material/Box'
import Toolbar from '@mui/material/Toolbar'
import 'material-icons/iconfont/outlined.css'
import { FullScreenLoadingProvider } from '@/context/FullScreenLoadingProvider'
import DebugBanner from '@/components/utils/DebugBanner'
import AlertProvider from '@/context/AlertProvider'
import { FullScreenLoading } from '@/components/progreso/FullScreenLoading'

export default function RootLayout({ children }: { children: ReactNode }) {
  return (
    <html lang="es">
      <body>
        <ThemeRegistry>
          <FullScreenLoadingProvider>
            <AlertProvider>
              <DebugBanner />
              <Box sx={{ display: 'flex' }}>
                <NavbarLogin />
                <Box component="main" sx={{ flexGrow: 1, p: 2 }}>
                  <Toolbar />
                  <Suspense
                    fallback={<FullScreenLoading mensaje={'Cargando...'} />}
                  >
                    {children}
                  </Suspense>
                </Box>
              </Box>
            </AlertProvider>
          </FullScreenLoadingProvider>
        </ThemeRegistry>
      </body>
    </html>
  )
}
