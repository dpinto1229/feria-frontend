import { Box, Button, Card } from '@mui/material'
import Typography from '@mui/material/Typography'
import { useForm } from 'react-hook-form'
import { LoginType } from '../types/loginTypes'
import { FormInputText } from 'src/components/form'
import ProgresoLineal from '@/components/progreso/ProgresoLineal'
import { useAuth } from '@/context/AuthProvider'

const LoginNormalContainer = () => {
  const { ingresar, progresoLogin } = useAuth()

  const { handleSubmit, control } = useForm<LoginType>({
    defaultValues: {
      usuario: 'ADMINISTRADOR',
      contrasena: '123',
    },
  })

  const iniciarSesion = async ({ usuario, contrasena }: LoginType) => {
    await ingresar({ usuario, contrasena })
  }

  return (
    <Card sx={{ borderRadius: 4, p: 4, maxWidth: '450px' }}>
      <form onSubmit={handleSubmit(iniciarSesion)}>
        <Box
          display={'grid'}
          justifyContent={'center'}
          alignItems={'center'}
          sx={{ borderRadius: 12 }}
        >
          <Typography
            align={'center'}
            color={'primary'}
            sx={{ flexGrow: 1, fontWeight: 'medium' }}
          >
            Iniciar Sesión
          </Typography>
          <Box sx={{ mt: 1, mb: 1 }}></Box>
          <FormInputText
            id={'usuario'}
            control={control}
            name="usuario"
            label="Usuario"
            size={'medium'}
            labelVariant={'subtitle1'}
            disabled={progresoLogin}
            rules={{ required: 'Este campo es requerido' }}
          />
          <Box sx={{ mt: 1, mb: 1 }}></Box>
          <FormInputText
            id={'contrasena'}
            control={control}
            name="contrasena"
            label="Contraseña"
            size={'medium'}
            labelVariant={'subtitle1'}
            type={'password'}
            disabled={progresoLogin}
            rules={{
              required: 'Este campo es requerido',
              minLength: {
                value: 3,
                message: 'Mínimo 3 caracteres',
              },
            }}
          />
          <Box sx={{ mt: 1, mb: 1 }}>
            <ProgresoLineal mostrar={progresoLogin} />
          </Box>
          <Box sx={{ height: 15 }}></Box>
          <Button
            type="submit"
            variant="contained"
            fullWidth
            disabled={progresoLogin}
          >
            <Typography sx={{ fontWeight: 'medium' }}>
              Iniciar sesión
            </Typography>
          </Button>
        </Box>
      </form>
    </Card>
  )
}

export default LoginNormalContainer
