'use client'
import Box from '@mui/material/Box'
import { Constantes } from '@/config/Constantes'
import {useEffect } from 'react'
import { imprimir } from '@/utils/imprimir'
import { Grid } from '@mui/material'
import LoginRegistroContainer from '@/app/login/ui/LoginRegistroContainer'
import LoginNormalContainer from './ui/LoginNormalContainer'


export default function LoginPage() {

  useEffect(() => {
    const getEstado = async () => {
      const res = await fetch(`${Constantes.baseUrl}/estado`)
      return res.json()
    }

    getEstado().then((value) => imprimir(value))
  }, [])

  return (
    <>
      <Box position="relative">
        <Grid container justifyContent="space-evenly" alignItems={'center'}>
          <Grid item xl={4} md={5} xs={12}>
            <Box display="flex" justifyContent="center" alignItems="center">
              <Box
                display={'flex'}
                justifyContent={'center'}
                alignItems={'center'}
                color={'primary'}
              >
                <LoginNormalContainer />
                {/* <LoginRegistroContainer /> */}
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </>
  )
}
