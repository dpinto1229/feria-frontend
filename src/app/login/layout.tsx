import { ReactNode } from 'react'
import { Constantes } from '@/config/Constantes'
import ThemeRegistry from '@/themes/ThemeRegistry'
import { AuthProvider } from '@/context/AuthProvider'
import { NavbarLogin } from '@/components/navbars/NavbarLogin'
import Box from '@mui/material/Box'
import Toolbar from '@mui/material/Toolbar'
import 'material-icons/iconfont/outlined.css'
import { FullScreenLoadingProvider } from '@/context/FullScreenLoadingProvider'
import DebugBanner from '@/components/utils/DebugBanner'
import AlertProvider from '@/context/AlertProvider'

export const metadata = {
  title: Constantes.siteName,
}

export default function RootLayout({ children }: { children: ReactNode }) {
  return (
    <html lang="es">
      <body>
        <ThemeRegistry>
          <FullScreenLoadingProvider>
            <AlertProvider>
              <DebugBanner />
              <AuthProvider>
                <Box sx={{ display: 'flex' }}>
                  <NavbarLogin />
                  <Box component="main" sx={{ flexGrow: 1, p: 2 }}>
                    <Toolbar />
                    {children}
                  </Box>
                </Box>
              </AuthProvider>
            </AlertProvider>
          </FullScreenLoadingProvider>
        </ThemeRegistry>
      </body>
    </html>
  )
}
