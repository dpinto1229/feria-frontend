import React, { useState, ReactNode } from 'react';
import { useSpring, animated } from 'react-spring';

interface AnimatedCardProps {
  children: ReactNode;
}

const AnimatedCard: React.FC<AnimatedCardProps> = ({ children }) => {
  const [hovered, setHovered] = useState(false);

  const cardAnimation = useSpring({
    transform: hovered ? 'scale(1.03)' : 'scale(1)',
  });

  return (
    <animated.div
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
      style={cardAnimation}
    >
        {children} 
    </animated.div>
  );
};

export default AnimatedCard;
