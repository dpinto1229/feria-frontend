import { Card, Grid, Typography, alpha, styled } from "@mui/material";
import CountUp from 'react-countup';
import { Icono } from "@/components/Icono";
import AnimatedCard from "./AnimatedCard";

interface CardsProps {
    icon: string;
    titulo: string;
    numero: number;
    color: string;
    colorIcono: string;
}

interface dataProps {
    data: CardsProps[];
}

const StyledIcon = styled('div')(({ theme }) => ({
    margin: 'auto',
    display: 'flex',
    borderRadius: '50%',
    alignItems: 'center',
    width: theme.spacing(8),
    height: theme.spacing(8),
    justifyContent: 'center',
    marginBottom: theme.spacing(3),
}));

export const CardComponent = ({ data }: dataProps) => {
    return (
        <Grid container direction={'row'} spacing={1}>
            {data?.map((card, index) => (
                <Grid key={index} item xs={12} sm={6} md={4}>
                    <CardItem card={card} />
                </Grid>
            ))}
        </Grid>
    )
}

const CardItem = ({ card }: { card: CardsProps }) => {
    return (
        <AnimatedCard>
            <Card
                variant='elevation'
                sx={{
                    py: 2,
                    border: (theme) => `1px solid ${theme.palette.divider}`,
                    textAlign: 'center',
                    borderRadius: 3,
                    position: 'relative',
                    cursor: "pointer",
                }}
            >
                <StyledIcon
                    sx={{
                        bgcolor: alpha(card.color, 0.15),
                        borderRadius: '80%',
                        position: 'absolute',
                        top: 0,
                        right: 0,
                        margin: '10px',
                    }}
                >
                    <Icono fontSize="large" style={{ color: card.color }}>
                        {card.icon}
                    </Icono>
                </StyledIcon>
                <Grid container direction={"column"} justifyContent={'start'} alignItems={'start'} paddingLeft={5}>
                    <Typography variant="h3" sx={{ marginBottom: '10px' }}>
                        <CountUp
                            end={card.numero}
                            duration={5}
                            style={{ fontSize: 40 }}
                        />
                    </Typography>
                    <Typography variant="caption" sx={{ opacity: 1 }}>
                        {card.titulo}
                    </Typography>
                </Grid>
            </Card>
        </AnimatedCard>
    );
}
