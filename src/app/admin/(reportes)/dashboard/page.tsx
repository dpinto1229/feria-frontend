'use client'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import { siteName, titleCase } from '@/utils'
import {
  Card,
  CardActionArea,
  CardContent,
  Grid,
  useMediaQuery,
  useTheme,
} from '@mui/material'
import { useAuth } from '@/context/AuthProvider'
import { useRouter } from 'next/navigation'
import { Icono } from '@/components/Icono'
import { Swiper, SwiperSlide } from 'swiper/react'
import { CardComponent } from './ui/CardComponent'
import AnimatedCard from './ui/AnimatedCard'
import Chart from 'react-google-charts'
import { useThemeContext } from '@/themes/ThemeRegistry'
import { useEffect, useState } from 'react'
import { useSession } from '@/hooks'
import { Constantes } from '@/config/Constantes'

export default function HomePage() {
  const { usuario, rolUsuario } = useAuth()
  const [cardsData, setCardsData] = useState<any[]>([])
  const [chartData, setChartData] = useState<any[]>([])
  const { sesionPeticion } = useSession()

  const theme = useTheme()
  const xs = useMediaQuery(theme.breakpoints.only('xs'))
  const { themeMode } = useThemeContext()
  const colors =
    themeMode === 'dark' ? ['#1565C0', '#9e9e9e'] : ['#2196F3', '#2196F3']

  const peticionCrads = async () => {
    try {
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/dashboard/cards`,
      })
      setCardsData(respuesta?.datos)
    } catch (error) {}
  }

  const peticionChart = async () => {
    try {
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/dashboard/charts`,
      })
      setChartData(respuesta?.datos)
    } catch (error) {}
  }

  const options = {
    backgroundColor: themeMode === 'dark' ? '#373635' : '#Fff',
    hAxis: {
      textStyle: {
        color: themeMode === 'dark' ? '#Fff' : '#000',
      },
    },
    vAxis: {
      textStyle: {
        color: themeMode === 'dark' ? '#Fff' : '#263238',
      },
      format: '0',
    },
    animation: {
      startup: true,
      easing: 'linear',
      duration: 1000,
    },
    legend: {
      textStyle: {
        color: themeMode === 'dark' ? '#Fff' : '#263238',
      },
      position: xs ? 'top' : 'right',
    },
    colors: colors,
  }

  useEffect(
    () => {
      peticionCrads()
      peticionChart()
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  )
  return (
    <>
      <title>{`Dahsboard - ${siteName()}`}</title>
      <Box>
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          alignItems="center"
        >
          <Grid>
            <Typography
              variant={'h5'}
              component="h1"
              sx={{ flexGrow: 1, fontWeight: 'medium' }}
            >
                Dashboard Informativo
            </Typography>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          {/* Cards */}
          <Grid item xs={12} sm={12} md={12}>
            <CardComponent data={cardsData} />
            {/* Gráfico */}
            <Box height={'20px'} />
            <Grid item xs={12} sm={12} md={12}>
              <AnimatedCard>
                <Card
                  sx={{
                    width: '100%',
                    height: '625px',
                    textAlign: 'left',
                    borderRadius: '20px',
                    border: (theme) => `1px solid ${theme.palette.divider}`,
                  }}
                >
                  <CardContent>
                    <Typography variant="h6" gutterBottom>
                      Grafico de seguimiento de empresas
                    </Typography>
                    {chartData ? (
                      <Chart
                        chartType={xs ? 'BarChart' : 'ColumnChart'}
                        width={'100%'}
                        height={'500px'}
                        data={chartData}
                        options={options}
                      />
                    ) : (
                      <Typography>Aún no se obtuvieron datos...</Typography>
                    )}
                  </CardContent>
                </Card>
              </AnimatedCard>
            </Grid>
          </Grid>
        </Grid>
        <Box height={'100px'} />
      </Box>
    </>
  )
}
