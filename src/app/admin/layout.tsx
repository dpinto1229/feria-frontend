'use client'
import Box from '@mui/material/Box'
import Toolbar from '@mui/material/Toolbar'
import ThemeRegistry from '@/themes/ThemeRegistry'
import { ReactNode, useEffect, useState } from 'react'
import { AuthProvider, useAuth } from '@/context/AuthProvider'
import 'material-icons/iconfont/outlined.css'
import { FullScreenLoadingProvider } from '@/context/FullScreenLoadingProvider'
import AlertProvider from '@/context/AlertProvider'
import { SideBarProvider, useSidebar } from '@/context/SideBarProvider'
import { Grid, useMediaQuery, useTheme } from '@mui/material'
import { NavbarUser } from '@/components/navbars/NavbarUser'
import { Sidebar } from '@/components/sidebar/Sidebar'
import { imprimir } from '@/utils/imprimir'
import DebugBanner from '@/components/utils/DebugBanner'

const Contenido = ({ children }: { children: ReactNode }) => {
  const { sideMenuOpen } = useSidebar()
  const [configurado, setConfigurado] = useState<boolean>(false)
  const { inicializarUsuario, estaAutenticado } = useAuth()

  const theme = useTheme()

  const sm = useMediaQuery(theme.breakpoints.only('sm'))
  const xs = useMediaQuery(theme.breakpoints.only('xs'))
  const md = useMediaQuery(theme.breakpoints.only('md'))

  useEffect(
    () => {
      if (!estaAutenticado)
        // TODO: Verificar cuando se solucione el problema
        inicializarUsuario()
          .then(() => {
            setConfigurado(true)
          })
          .catch(imprimir)
          .finally(() => {
            imprimir('Verificación de login finalizada 👨‍💻')
          })
    }, // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  )

  return (
    <>
      {configurado && <Sidebar />}
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justifyContent="center"
        justifyItems={'center'}
      >
        <Box sx={{ display: 'flex' }}>
          <NavbarUser />
        </Box>
        <Box
          component="main"
          sx={{
            width: sm || xs || md ? '100%' : sideMenuOpen ? '80%' : '100%',
            // backgroundColor: 'primary.main',
            display: 'flex',
            flexDirection: 'column',
            ml: sm || xs || md ? '0%' : sideMenuOpen ? '200px' : '0%',
            transition: 'all 0.2s ease-out !important',
          }}
        >
          <Toolbar />
          <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justifyContent="initial"
            justifyItems={'center'}
            style={{ minHeight: '80vh' }}
          >
            <div
              style={{
                height: '75vh',
                width: xs || sm ? '90%' : '95%',
              }}
            >
              <Box height={'30px'} />
              {configurado && children}
            </div>
          </Grid>
        </Box>
      </Grid>
    </>
  )
}

export default function RootLayout({ children }: { children: ReactNode }) {
  return (
    <html lang="es">
      <body>
        <ThemeRegistry>
          <FullScreenLoadingProvider>
            <AlertProvider>
              <DebugBanner />
              <AuthProvider>
                <SideBarProvider>
                  <Contenido>{children}</Contenido>
                </SideBarProvider>
              </AuthProvider>
            </AlertProvider>
          </FullScreenLoadingProvider>
        </ThemeRegistry>
      </body>
    </html>
  )
}
