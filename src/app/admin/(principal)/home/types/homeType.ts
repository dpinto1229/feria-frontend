import { Constantes } from '@/config/Constantes'

export const imagenesBanner = [
  {
    id: 1,
    url: `${Constantes.sitePath}/feria/img1.png`,
  },
  {
    id: 2,
    url: `${Constantes.sitePath}/feria/img2.jpeg`,
  },
  {
    id: 3,
    url: `${Constantes.sitePath}/feria/img3.jpeg`,
  },
  {
    id: 4,
    url: `${Constantes.sitePath}/feria/img4.jpeg`,
  },
  {
    id: 5,
    url: `${Constantes.sitePath}/feria/img5.jpeg`,
  },
  {
    id: 6,
    url: `${Constantes.sitePath}/feria/img6.jpeg`,
  },
  {
    id: 7,
    url: `${Constantes.sitePath}/feria/img7.jpeg`,
  },
  {
    id: 8,
    url: `${Constantes.sitePath}/feria/img8.jpeg`,
  },
  {
    id: 9,
    url: `${Constantes.sitePath}/feria/img9.jpeg`,
  },
  {
    id: 10,
    url: `${Constantes.sitePath}/feria/img10.jpeg`,
  },
  {
    id: 11,
    url: `${Constantes.sitePath}/feria/img11.jpeg`,
  },
  {
    id: 12,
    url: `${Constantes.sitePath}/feria/img12.jpeg`,
  },
]

export const datosEmpresa = [
  {
    id: 1,
    titulo: 'Reseña Histórica ',
    icono: 'history',
    contenido:
      'Teniendo amplia experiencia en el área constituyéndose en una empresa unipersonal la Familia Irusta y Medic unieron capitales para organizar una feria que tuvo su primera versión el año 2022. Ahora en su segunda versión realizaron la feria antes dicha con éxito.',
  },
  {
    id: 2,
    titulo: 'Misión',
    icono: 'military_tech',
    contenido:
      'Apostamos por el crecimiento coherente y sostenido, mediante la actividad ferial, proponiendo un calendario variado y diverso que priorice eventos de carácter innovador y diferenciado.',
  },
  {
    id: 3,
    titulo: 'Visión',
    icono:'visibility',
    contenido:
      'Ser una empresa reconocida por convertir a nuestro departamento en un referente de negocios feriales, encontrándose a la vanguardia por ser inclusiva y disruptiva.',
  },
  {
    id: 4,
    titulo: 'Valores',
    icono:'emoji_people',
    contenido: 'Responsabilidad, compromiso, juventud e innovación.',
  },
  {
    id: 5,
    titulo: 'Filosofía',
    icono:'psychology',
    contenido:
      'La empresa tiene una solida creencia en valores cristianos apoyados por su esfuerzo y fe en ellos.',
  },
]
