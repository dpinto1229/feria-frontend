'use client'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import { siteName, titleCase } from '@/utils'
import { Card, CardActionArea, CardContent, Grid } from '@mui/material'
import { useAuth } from '@/context/AuthProvider'
import { useRouter } from 'next/navigation'
import { Icono } from '@/components/Icono'
import { Swiper, SwiperSlide } from 'swiper/react'

// Import Swiper styles
import 'swiper/css'
import 'swiper/css/pagination'
import 'swiper/css/navigation'

import './styles.css'

import { Pagination, Navigation, Autoplay } from 'swiper/modules'

import { Constantes } from '@/config/Constantes'
import { datosEmpresa, imagenesBanner } from './types/homeType'
import Image from 'next/image'

export default function HomePage() {
  const { usuario, rolUsuario } = useAuth()

  const router = useRouter()

  return (
    <>
      <title>{`Home - ${siteName()}`}</title>
      <Box>
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          alignItems="center"
        >
          <Grid>
            <Typography
              variant={'h5'}
              component="h1"
              sx={{ flexGrow: 1, fontWeight: 'medium' }}
            >
              Bienvenid@ {titleCase(usuario?.persona?.nombres ?? '')}{' '}
              {titleCase(
                usuario?.persona?.primerApellido ??
                  usuario?.persona?.segundoApellido ??
                  ''
              )}
            </Typography>
            <Typography variant={'subtitle2'}>{rolUsuario?.nombre}</Typography>
          </Grid>
        </Grid>
        <Grid>
          <Box height={'20px'} />
          <Typography sx={{ fontSize: 14 }}>
            A continuación, se muestran las imagenes mas destacadas
          </Typography>
          <Box height={'5px'} />
        </Grid>
        <Swiper
          spaceBetween={30}
          autoplay={{
            delay: 5000,
            disableOnInteraction: false,
          }}
          pagination={{
            clickable: true,
          }}
          loop={true}
          navigation={true}
          modules={[Autoplay, Pagination, Navigation]}
          //cambismo el estilo del swiper de azul a blanco
          style={{ backgroundColor: 'white', borderRadius: 10 }}
          className="mySwiper"
        >
          {imagenesBanner.map((imagen, index) => (
            <SwiperSlide
              key={`imagen-${index}`}
              style={{
                backgroundImage: `url(${imagen.url})`,
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                height: '500px',
              }}
            ></SwiperSlide>
          ))}
        </Swiper>
        <Box height={'20px'} />
        <Typography sx={{ fontSize: 18, textAlign: 'center', fontWeight: 600 }}>
          Datos de la empresa
        </Typography>
        <Box height={'20px'} />
        <Grid
          container
          rowSpacing={1}
          columnSpacing={{ xs: 1, sm: 2, md: 3 }}
          textAlign={'center'}
        >
          {datosEmpresa.map((dato, index) => (
            <Grid item xs={12} sm={6} md={4} key={`datoEmpresa-${index}`}>
              <Card
                variant={'outlined'}
                sx={{
                  borderRadius: 2,
                  padding: 2,
                }}
              >
                <CardContent>
                  <Icono fontSize="medium">{dato.icono}</Icono>
                  <Typography
                    variant={'h6'}
                    sx={{ fontSize: 16, fontWeight: 600 }}
                  >
                    {dato.titulo}
                  </Typography>
                  <Box height={'5px'} />
                  <Typography
                    variant={'body2'}
                    sx={{
                      fontSize: 14,

                      //justificamos el texto
                      textAlign: 'justify',
                    }}
                  >
                    {dato.contenido}
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          ))}
        </Grid>
        <Box height={'20px'} />
        <Typography sx={{ fontSize: 14, fontWeight: 500 }}>
          Módulos que tiene acceso
        </Typography>
        <Grid container direction="row" alignItems="center">
          <Grid container direction="row">
            {usuario?.roles
              .filter((ur) => ur.idRol == rolUsuario?.idRol)
              .map((rolUsuario, index) => (
                <div key={`usuario-${index}`}>
                  {rolUsuario.modulos.map((modulo, index1) => (
                    <Grid
                      container
                      direction="row"
                      key={`rolUsuario-${index}-${index1}`}
                    >
                      <Grid>
                        <Box height={'20px'} />
                        <Typography sx={{ fontSize: 14, fontWeight: 'medium' }}>
                          {modulo.label}
                        </Typography>
                        <Box height={'20px'} />
                      </Grid>
                      <Grid
                        container
                        direction="row"
                        spacing={{ xs: 2, md: 3 }}
                        columns={{ xs: 2, sm: 8, md: 12, xl: 12 }}
                      >
                        {modulo.subModulo.map((subModulo, index2) => (
                          <Grid
                            item
                            xs={2}
                            sm={4}
                            md={4}
                            key={`$subModulo-${index}-${index1}-${index2}`}
                          >
                            <CardActionArea
                              sx={{
                                borderRadius: 3,
                              }}
                              onClick={() => {
                                router.push(subModulo.url)
                              }}
                            >
                              <Card
                                variant={'outlined'}
                                sx={{
                                  borderRadius: 3,
                                }}
                              >
                                <CardContent>
                                  <Grid container direction="row">
                                    <Icono>{subModulo.propiedades.icono}</Icono>
                                    <Box height={'30px'} width={'10px'} />
                                    <Typography
                                      variant="caption"
                                      sx={{
                                        fontSize: 14,
                                        fontWeight: 'medium',
                                      }}
                                    >
                                      {`${subModulo.label}`}
                                    </Typography>
                                  </Grid>
                                  <Typography
                                    variant="body2"
                                    sx={{ fontSize: 14 }}
                                  >
                                    {`${subModulo.propiedades.descripcion}`}
                                  </Typography>
                                </CardContent>
                              </Card>
                            </CardActionArea>
                          </Grid>
                        ))}
                      </Grid>
                    </Grid>
                  ))}
                </div>
              ))}
          </Grid>
        </Grid>
        <Box height={'100px'} />
      </Box>
    </>
  )
}
