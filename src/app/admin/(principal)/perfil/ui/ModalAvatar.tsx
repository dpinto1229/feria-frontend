import { Box, Button, DialogActions, DialogContent, Grid } from '@mui/material'
import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { cambioPassForm, cambioPassPeticion } from './perfilTypes'
import Typography from '@mui/material/Typography'
import { useAlerts, useSession } from '@/hooks'
import { encodeBase64, InterpreteMensajes, seguridadPass } from '@/utils'
import { Constantes } from '@/config/Constantes'
import { FormInputText } from 'src/components/form'
import { NivelSeguridadPass } from '@/components/utils/NivelSeguridadPass'
import ProgresoLineal from '@/components/progreso/ProgresoLineal'
import { FormInputAvatarDropdown } from '@/components/form/FormInputAvatarDropDown'
import { useAuth } from '@/context/AuthProvider'

export interface CambioPassModalType {
  accionCorrecta: () => void
  accionCancelar: () => void
}

export const VistaModalAvatar = ({
  accionCorrecta,
  accionCancelar,
}: CambioPassModalType) => {
  const { Alerta } = useAlerts()
  const [loadingModal, setLoadingModal] = useState<boolean>(false)
  const { usuario } = useAuth()
  const { handleSubmit, control } = useForm<any>({
    defaultValues: {
      avatar: usuario?.avatar || null || undefined,
    },
  })

  const { sesionPeticion } = useSession()

  const actualizarDatos = async (params: any) => {
    try {
      setLoadingModal(true)
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/usuarios/perfil/${usuario?.id}`,
        tipo: 'patch',
        body: params,
      })
      Alerta({ mensaje: InterpreteMensajes(respuesta), variant: 'success' })
      accionCorrecta()
    } catch (e) {
      Alerta({ mensaje: InterpreteMensajes(e), variant: 'error' })
    } finally {
      setLoadingModal(false)
    }
  }

  const avatarOptions = [
    '../avatars/avatar1.jpg',
    '../avatars/avatar2.jpg',
    '../avatars/avatar3.jpg',
  ]

  return (
    <form onSubmit={handleSubmit(actualizarDatos)}>
      <DialogContent dividers>
        <Grid container direction={'column'} justifyContent="space-evenly">
          <Box height={'5px'} />
          <Typography sx={{ fontWeight: 'medium' }} variant={'subtitle2'}>
            ¡Bienvenido! Para mejorar tu experiencia, nos gustaría saber un poco
            que avatar te identifica.
          </Typography>
          <Box height={'20px'} />
          <Grid container direction="row" spacing={{ xs: 2, sm: 1, md: 2 }}>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputAvatarDropdown
                id="avatar-select"
                name="avatar"
                control={control}
                label="Selecciona tu avatar"
                size="medium"
                rules={{ required: 'Campo requerido' }}
                disabled={false}
                options={avatarOptions}
              />
            </Grid>
            {/* <Grid item xs={12} sm={8} md={8}>
                <FormInputFile
                  id="archivo"
                  name="file"
                  control={control}
                  label="Cargar Imagen de Usuario"
                  tiposPermitidos={['image/jpeg', 'image/png']}
                  limite={1}
                  multiple={true}
                />
              </Grid> */}
            <Box height={'5px'} />
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions
        sx={{
          my: 1,
          mx: 2,
          justifyContent: {
            lg: 'flex-end',
            md: 'flex-end',
            xs: 'center',
            sm: 'center',
          },
        }}
      >
        <Button
          variant={'outlined'}
          disabled={loadingModal}
          onClick={accionCancelar}
        >
          Cancelar
        </Button>
        <Button type={'submit'} variant={'contained'} disabled={loadingModal}>
          Actualizar
        </Button>
      </DialogActions>
    </form>
  )
}
