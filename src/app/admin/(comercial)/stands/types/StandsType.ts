export interface BloqueType {
  id: string
  nombre: string
  descripcion: string
  imagen: string
  plantas: PlantaType[]
}

export interface PlantaType {
  id: string
  nivel: string
  imagen: string
  rutaModelo?: string
  stands: StandType[]
}

export interface StandType {
  id: string
  codigo: string
  area: string
  precio: number
  color: string
  ubicacionX: number
  ubicacionY: number
  estado: string
  idPlanta: string
  plantas: PlantaType
  reservas: ReservaType[]
}

export interface ReservaType {
  id: string
  monto: number
  idStand: string
  idEmpresa: string
  empresa: EmpresaType
  fechaCreacion: string
}

export interface EmpresaType {
  nombreEmpresa: string
}
