/* eslint-disable @next/next/no-img-element */
import React, { useState } from 'react'
import {
  Box,
  Button,
  CardActionArea,
  CardContent,
  DialogActions,
  DialogContent,
  Grid,
  Typography,
} from '@mui/material'

export interface ModalParametroType {
  parametro?: any | null
  accionCorrecta?: () => void
  accionCancelar: () => void
}

export const VistaModalStands = ({
  parametro,
  accionCorrecta,
  accionCancelar,
}: ModalParametroType) => {
  const [loadingModal, setLoadingModal] = useState<boolean>(false)

  return (
    <>
      <DialogContent dividers>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={12} md={12}>
            <Box>
              <CardContent
                sx={{
                  border: '1px solid transparent',
                  transition: 'transform 0.2s, box-shadow 0.2s',
                  m: 1,
                  '&:hover': {
                    transform: 'scale(1.03)',
                  },
                }}
              >
                <CardActionArea>
                  <Box
                    display="flex"
                    flexDirection="column"
                    alignItems="center"
                    mb={1}
                  >
                    <Box
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                    >
                      <img
                        src={parametro?.imagen}
                        alt={parametro?.nombre}
                        width="150"
                        height="150"
                      />
                    </Box>
                    <Box sx={{ height: '10px' }} />

                    <Box
                      mt={1}
                      textAlign="center"
                      overflow="auto"
                      maxHeight="100px"
                    >
                      <Typography
                        variant="body1"
                        sx={{ fontSize: '16px', fontWeight: 600 }}
                      >
                        {parametro?.nombre}
                      </Typography>
                      <Typography
                        variant="body2"
                        sx={{ fontSize: '14px', fontWeight: 400 }}
                      >
                        {parametro?.descripcion}
                      </Typography>
                    </Box>
                  </Box>
                </CardActionArea>
              </CardContent>
            </Box>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions
        sx={{
          my: 1,
          mx: 2,
          justifyContent: {
            lg: 'flex-end',
            md: 'flex-end',
            xs: 'center',
            sm: 'center',
          },
        }}
      >
        <Button
          variant={'outlined'}
          disabled={loadingModal}
          onClick={accionCancelar}
        >
          Cerrar
        </Button>
        <Button
          variant={'contained'}
          disabled={loadingModal}
          onClick={() => {
            window.open(parametro?.url, '_blank')
          }}
        >
          Ver en 3D
        </Button>
      </DialogActions>
    </>
  )
}
