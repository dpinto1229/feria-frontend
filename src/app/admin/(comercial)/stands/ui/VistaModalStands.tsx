import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import {
  Box,
  Button,
  Card,
  DialogActions,
  DialogContent,
  Grid,
  Typography,
} from '@mui/material'
import { useAlerts, useSession } from '@/hooks'
import { delay, InterpreteMensajes } from '@/utils'
import { Constantes } from '@/config/Constantes'
import { imprimir } from '@/utils/imprimir'
import { FormInputDropdown, FormInputText } from 'src/components/form'
import ProgresoLineal from '@/components/progreso/ProgresoLineal'
import { StandType } from '../types/StandsType'
import dayjs from 'dayjs'

export interface StandsModalType {
  idPlanta?: string
  stands?: StandType | null
  ubicacionX?: number
  ubicacionY?: number
  accionCorrecta: () => void
  accionCancelar: () => void
}

export const VistaModalStand = ({
  idPlanta,
  stands,
  ubicacionX,
  ubicacionY,
  accionCorrecta,
  accionCancelar,
}: StandsModalType) => {
  const [loadingModal, setLoadingModal] = useState<boolean>(false)
  const [modo, setModo] = useState<'editar' | 'reservar'>('editar')
  const [empresas, setEmpresas] = useState<any[]>([])
  const { Alerta } = useAlerts()
  const { sesionPeticion } = useSession()

  const montoStands = [
    { monto: 4000, area: 9 },
    { monto: 8000, area: 18 },
    { monto: 10000, area: 24 },
    { monto: 12000, area: 27 },
    { monto: 15000, area: 36 },
    { monto: 20000, area: 72 },
  ]

  const {
    handleSubmit: hadleSubitEditar,
    control: controlEditar,
    setValue,
    watch: watch,
  } = useForm<StandType>({
    defaultValues: {
      id: stands?.id,
      codigo: stands?.codigo,
      area: stands?.area,
      color: stands?.color,
      precio: stands?.precio,
      idPlanta: idPlanta,
      ubicacionX: stands?.ubicacionX || ubicacionX,
      ubicacionY: stands?.ubicacionY || ubicacionY,
    },
  })

  const {
    handleSubmit: hadleSubitReservar,
    control: controlReservar,
    watch: watchReserva,
  } = useForm<any>({})

  const fetchEmpresas = async () => {
    try {
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/empresas`,
      })
      setEmpresas(respuesta.datos.filas)
    } catch (error) {
      imprimir(`Error al editar stand`, error)
      Alerta({ mensaje: `${InterpreteMensajes(error)}`, variant: 'error' })
    }
  }
  // Función para manejar el modo editar
  const onSubmitEditar = async (data: StandType) => {
    try {
      setLoadingModal(true)
      await delay(1000)
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/stands${data.id ? `/${data.id}` : ''}`,
        tipo: !!data.id ? 'patch' : 'post',
        body: {
          ...data,
          idPlanta: idPlanta,
          ubicacionX: ubicacionX,
          ubicacionY: ubicacionY,
        },
      })
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: 'success',
      })
      accionCorrecta()
    } catch (e) {
      imprimir(`Error al editar stand`, e)
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: 'error' })
    } finally {
      setLoadingModal(false)
    }
  }

  // Función para manejar el modo reservar
  const onSubmitReservar = async (data: any) => {
    try {
      setLoadingModal(true)
      await delay(1000)
      const body = {
        monto: data.monto,
        idEmpresa: data.idEmpresa,
        idStand: stands?.id,
        descuento: data.descuento,
      }
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/stands/reservar`,
        tipo: 'post',
        body: body,
      })
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: 'success',
      })
      accionCorrecta()
    } catch (e) {
      imprimir(`Error al reservar stand`, e)
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: 'error' })
    } finally {
      setLoadingModal(false)
    }
  }

  const eliminarStand = async () => {
    try {
      setLoadingModal(true)
      await delay(1000)
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/stands/eliminar/${stands?.id}`,
        tipo: 'patch',
      })
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: 'success',
      })
      accionCorrecta()
    } catch (e) {
      imprimir(`Error al eliminar stand`, e)
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: 'error' })
    } finally {
      setLoadingModal(false)
    }
  }

  useEffect(() => {
    fetchEmpresas()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const areaValue = watch('area') // Para obtener el valor del campo 'area'
  const precio = watch('precio')
  const descuento = watchReserva('descuento')
  const precioConDescuento = +precio - +descuento

  useEffect(() => {
    if (+precio && +descuento) {
      setValue('precio', precioConDescuento > 0 ? precioConDescuento : 0)
    }
    if (!areaValue || areaValue === '') {
      setValue('precio', 0) // Si el campo de área está vacío, establecemos el precio a 0
    } else {
      const stand = montoStands.find(
        (montoStand) => montoStand.area === Number(areaValue)
      )
      if (stand) {
        setValue('precio', stand.monto) // Actualizamos el precio si el área coincide
      } else {
        setValue('precio', stands?.precio ?? 0) // Si no hay coincidencia, ponemos el precio en 0
      }
    }
  }, [areaValue, setValue, precio, descuento])

  return (
    <>
      <DialogContent dividers>
        {stands?.estado === 'RESERVADO' && (
          //mostramos un card con la información de la empresa que reservó el stand
          <Box
            display="flex"
            justifyContent="center"
            alignItems="center"
            flexDirection="column"
            p={2}
            border={1}
            borderColor="primary.main"
            borderRadius={5}
          >
            <Typography>
              Reservado por: {stands?.reservas[0].empresa.nombreEmpresa}
            </Typography>
            <Typography>Monto: {stands?.reservas[0].monto} Bs</Typography>
            <Typography>
              Fecha de reserva:{' '}
              {dayjs(stands?.reservas[0].fechaCreacion).format(
                'DD/MM/YYYY HH:mm'
              )}
            </Typography>
          </Box>
        )}
        {stands?.estado !== 'RESERVADO' && stands && (
          <Box mt={2} display="flex" justifyContent="center">
            <Button
              variant="text"
              onClick={() => setModo(modo === 'editar' ? 'reservar' : 'editar')}
            >
              Cambiar a Modo {modo === 'editar' ? 'Reservar' : 'Editar'} stand
            </Button>
          </Box>
        )}
        {modo === 'editar' && stands?.estado !== 'RESERVADO' && (
          <Grid container direction={'column'} justifyContent="space-evenly">
            <Grid container direction="row" spacing={{ xs: 2, sm: 1, md: 2 }}>
              <Grid item xs={12} sm={12} md={6}>
                <FormInputText
                  id={'codigo'}
                  control={controlEditar}
                  name="codigo"
                  label="Código"
                  disabled={loadingModal}
                  rules={{ required: 'Este campo es requerido' }}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <FormInputText
                  id={'area'}
                  control={controlEditar}
                  name="area"
                  label="Area (m2)"
                  type="number"
                  disabled={loadingModal}
                  rules={{ required: 'Este campo es requerido' }}
                />
              </Grid>
            </Grid>
            <Box height={'15px'} />
            <Grid container direction="row" spacing={{ xs: 2, sm: 1, md: 2 }}>
              <Grid item xs={12} sm={12} md={6}>
                <FormInputText
                  id={'precio'}
                  control={controlEditar}
                  name="precio"
                  label="Precio (Bs)"
                  type="number"
                  disabled={loadingModal}
                  rules={{ required: 'Este campo es requerido' }}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <FormInputText
                  id={'color'}
                  control={controlEditar}
                  name="color"
                  label="Color"
                  type="color"
                  disabled={loadingModal}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <FormInputText
                  id={'ubicacionX'}
                  control={controlEditar}
                  name="ubicacionX"
                  label="Ubicación X"
                  disabled={loadingModal || !stands}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <FormInputText
                  id={'ubicacionY'}
                  control={controlEditar}
                  name="ubicacionY"
                  label="Ubicación Y"
                  disabled={loadingModal || !stands}
                />
              </Grid>
            </Grid>
            <Box height={'10px'} />
            <ProgresoLineal mostrar={loadingModal} />
            <Box height={'5px'} />
          </Grid>
        )}
        {modo === 'reservar' && stands?.estado !== 'RESERVADO' && (
          <Grid container direction={'column'} justifyContent="space-evenly">
            <Card>
              <Box>
                <Typography variant="h6">{`Precio del stand: ${
                  isNaN(precioConDescuento)
                    ? stands?.precio
                    : precioConDescuento
                } Bs`}</Typography>
              </Box>
            </Card>
            <Grid container direction="row" spacing={{ xs: 2, sm: 1, md: 2 }}>
              <Grid item xs={12} sm={12} md={12}>
                <FormInputText
                  id={'monto'}
                  control={controlReservar}
                  name="monto"
                  label="Monto de reserva (Bs)"
                  type="number"
                  disabled={loadingModal}
                  rules={{ required: 'Este campo es requerido' }}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <FormInputText
                  id={'descuento'}
                  control={controlReservar}
                  name="descuento"
                  label="Descuento (Bs)"
                  type="number"
                  disabled={loadingModal}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <FormInputDropdown
                  id={'idEmpresa'}
                  control={controlReservar}
                  name="idEmpresa"
                  label="Empresas"
                  disabled={loadingModal}
                  options={empresas.map((entidad: any) => ({
                    value: entidad.id,
                    key: entidad.id,
                    label: entidad.nombreEmpresa,
                  }))}
                  rules={{ required: 'Este campo es requerido' }}
                />
              </Grid>
            </Grid>
            <Box height={'15px'} />
            <ProgresoLineal mostrar={loadingModal} />
            <Box height={'5px'} />
          </Grid>
        )}
      </DialogContent>
      <DialogActions
        sx={{
          my: 1,
          mx: 2,
          justifyContent: {
            lg: 'flex-end',
            md: 'flex-end',
            xs: 'center',
            sm: 'center',
          },
        }}
      >
        {stands?.id && (
          <Button
            variant="contained"
            disabled={loadingModal}
            sx={{ backgroundColor: 'red', color: 'white' }}
            onClick={eliminarStand}
          >
            Eliminar Stand
          </Button>
        )}
        <Button
          variant={'outlined'}
          disabled={loadingModal}
          onClick={accionCancelar}
        >
          Cancelar
        </Button>

        {modo === 'editar' && stands?.estado !== 'RESERVADO' && (
          <Button
            variant={'contained'}
            disabled={loadingModal}
            onClick={hadleSubitEditar(onSubmitEditar)}
          >
            Guardar
          </Button>
        )}
        {modo === 'reservar' && stands?.estado !== 'RESERVADO' && (
          <Button
            variant={'contained'}
            disabled={loadingModal}
            onClick={hadleSubitReservar(onSubmitReservar)}
          >
            Reservar
          </Button>
        )}
      </DialogActions>
    </>
  )
}
