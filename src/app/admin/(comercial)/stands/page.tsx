/* eslint-disable @next/next/no-img-element */
'use client'
import { CustomDialog } from '@/components/modales/CustomDialog'
import { Constantes } from '@/config/Constantes'
import { InterpreteMensajes, siteName } from '@/utils'
import {
  Box,
  CardActionArea,
  CardContent,
  Grid,
  Typography,
  useMediaQuery,
  useTheme,
} from '@mui/material'
import { useEffect, useState } from 'react'
import { VistaModalStands } from './ui/ModalDetalleStand'
import { IconoBoton } from '@/components/botones/IconoBoton'
import { useAlerts, useSession } from '@/hooks'
import { IconoTooltip } from '@/components/botones/IconoTooltip'
import { BloqueType } from './types/StandsType'
import { usePathname, useRouter } from 'next/navigation'

export default function ParametrosPage() {
  const [abrirDetalle, setAbrirDetalle] = useState(false)
  const { sesionPeticion } = useSession()
  const router = useRouter()
  const path = usePathname()
  const { Alerta } = useAlerts()

  const [standsData, setStandsData] = useState<BloqueType[]>([])
  const fetchDatos = async () => {
    try {
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/stands/bloques`,
      })
      setStandsData(respuesta.datos?.filas)
    } catch (error) {
      Alerta({
        mensaje: `${InterpreteMensajes(error)}`,
        variant: 'error',
      })
    }
  }

  const abrirDetalleModal = (stand: any) => {
    setStandsData(stand)
    setAbrirDetalle(true)
  }
  const cerrarDetalleModal = () => {
    setAbrirDetalle(false)
  }

  const theme = useTheme()
  const xs = useMediaQuery(theme.breakpoints.only('xs'))

  useEffect(
    () => {
      fetchDatos()
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  )

  return (
    <>
      <title>{`Gestión de bloques - ${siteName()}`}</title>
      <Typography variant="h6" gutterBottom>
        Gestión de bloques
      </Typography>
      <Box
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
        }}
      >
        <IconoTooltip
          id={'actualizarUsuario'}
          titulo={'Actualizar'}
          key={`actualizarUsuario`}
          accion={async () => {
            await fetchDatos()
          }}
          icono={'refresh'}
          name={'Actualizar lista de usuario'}
        />
        <IconoBoton
          id={'armar-stand'}
          key={'armar-stand'}
          texto={'Ejemplo de Stand'}
          variante={xs ? 'icono' : 'boton'}
          icono={'add_circle_outline'}
          descripcion={'Ejem: Armar un nuevo stand.'}
          accion={() => {
            window.open(
              'https://home.by.me/es/project/kevincho10916038-2218/cafeteria-mini-breakfast?open_planner=true',
              '_blank'
            )
          }}
        />
      </Box>

      <Grid container spacing={2}>
        {standsData?.map((stand, index) => (
          <Grid key={index} item xs={6} sm={6} md={3}>
            <Box>
              <CardContent
                sx={{
                  border: `1px solid ${theme.palette.divider}`,
                  borderRadius: 5,
                  m: 1,
                  '&:hover': {
                    transform: 'scale(1.03)',
                  },
                }}
              >
                <CardActionArea
                  onClick={() => router.push(`${path}/${stand.id}`)}
                >
                  <Box
                    display="flex"
                    flexDirection="column"
                    alignItems="center"
                    mb={1}
                  >
                    <Box
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                    >
                      <img
                        src={stand.imagen}
                        alt={stand.nombre}
                        style={{
                          width: '100%',
                          height: '100%',
                          borderRadius: '5px',
                        }}
                      />
                    </Box>
                    <Box sx={{ height: '10px' }} />
                    <Box
                      mt={1}
                      textAlign="center"
                      overflow="auto"
                      maxHeight="100px"
                    >
                      <Typography
                        variant="body1"
                        sx={{ fontSize: '16px', fontWeight: 600 }}
                      >
                        {stand.nombre}
                      </Typography>
                      <Typography
                        variant="body2"
                        sx={{ fontSize: '14px', fontWeight: 400 }}
                      >
                        {stand.descripcion}
                      </Typography>
                    </Box>
                  </Box>
                </CardActionArea>
              </CardContent>
            </Box>
          </Grid>
        ))}
      </Grid>
      <CustomDialog
        isOpen={abrirDetalle}
        handleClose={cerrarDetalleModal}
        title="Detalle Stands"
      >
        <VistaModalStands
          parametro={standsData}
          accionCancelar={cerrarDetalleModal}
        />
      </CustomDialog>
    </>
  )
}
