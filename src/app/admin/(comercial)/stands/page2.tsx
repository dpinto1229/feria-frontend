'use client'
import { useEffect, useState } from 'react'
import { Box, Typography, useTheme } from '@mui/material'
import { Constantes } from '@/config/Constantes'
import { useSession } from '@/hooks'
import { imprimir } from '@/utils/imprimir'
import { IconoTooltip } from '@/components/botones/IconoTooltip'
import { CustomDialog } from '@/components/modales/CustomDialog'
import { VistaModalStand } from './ui/VistaModalStands'
import { siteName } from '@/utils'

export interface Stand {
  id: string
  nombre: string
  bloque: string
  codigo: string
  estado: string
  piso: string
  precio: string
  area: string
  color: string
  ubicacionX: number
  ubicacionY: number
}

const StandsCustom = () => {
  const { sesionPeticion } = useSession()
  const [standsData, setStandsData] = useState<Stand[]>([])
  const [modalStand, setModalStand] = useState(false)
  const [ubicacionX, setUbicacionX] = useState(0)
  const [ubicacionY, setUbicacionY] = useState(0)
  const [selectedStand, setSelectedStand] = useState<Stand | null>(null)
  const [isEditing, setIsEditing] = useState(false) // Nuevo estado para determinar el modo del modal
  const [cursorStyle, setCursorStyle] = useState<React.CSSProperties>({
    cursor: 'default',
  }) // Nuevo estado para el cursor
  const theme = useTheme()

  const abrirModal = () => {
    setModalStand(true)
  }

  const cerrarModal = () => {
    setModalStand(false)
    setSelectedStand(null)
  }

  const fetchStands = async () => {
    try {
      const response = await sesionPeticion({
        url: `${Constantes.baseUrl}/stands/bloques`,
      })
      setStandsData(response.datos?.filas)
    } catch (error) {
      imprimir('Error fetching stands:', error)
    }
  }

  useEffect(() => {
    fetchStands()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const handleSVGClick = (event: React.MouseEvent<SVGSVGElement>) => {
    const rect = event.currentTarget.getBoundingClientRect()
    const x = event.clientX - rect.left
    const y = event.clientY - rect.top
    setUbicacionX(x)
    setUbicacionY(y)

    // Comprobar si se hizo clic en un stand existente
    const clickedStand = standsData.find((stand) => {
      const area = parseFloat(stand.area)
      const standX = stand.ubicacionX - Math.sqrt(area) * 5
      const standY = stand.ubicacionY - Math.sqrt(area) * 5
      return (
        x >= standX &&
        x <= standX + Math.sqrt(area) * 10 &&
        y >= standY &&
        y <= standY + Math.sqrt(area) * 10
      )
    })

    if (clickedStand) {
      setSelectedStand(clickedStand)
      setIsEditing(true)
    } else {
      setSelectedStand(null)
      setIsEditing(false)
    }

    abrirModal()
  }

  const handleMouseEnter = () => {
    setCursorStyle({ cursor: 'pointer' }) // Cambiar el cursor a pointer
  }

  const handleMouseLeave = () => {
    setCursorStyle({ cursor: 'default' }) // Volver a cursor por defecto
  }

  const renderGrid = (width: number, height: number) => {
    const gridSize = 50 // Ajusta el tamaño de la cuadrícula
    const gridLines = []

    // Dibujar líneas verticales
    for (let x = 0; x <= width; x += gridSize) {
      gridLines.push(
        <line
          key={`v-${x}`}
          x1={x}
          y1={0}
          x2={x}
          y2={height}
          stroke="#e0e0e0"
          strokeWidth={1}
        />
      )
    }

    // Dibujar líneas horizontales
    for (let y = 0; y <= height; y += gridSize) {
      gridLines.push(
        <line
          key={`h-${y}`}
          x1={0}
          y1={y}
          x2={width}
          y2={y}
          stroke="#e0e0e0"
          strokeWidth={1}
        />
      )
    }

    return gridLines
  }

  return (
    <>
      <title>{`Gestión de stands - ${siteName()}`}</title>
      <Typography variant="h6" gutterBottom>
        Gestión de Stands
      </Typography>
      <Box
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
        }}
      >
        <IconoTooltip
          id={'actualizarUsuario'}
          titulo={'Actualizar'}
          key={`actualizarUsuario`}
          accion={async () => {
            await fetchStands()
          }}
          icono={'refresh'}
          name={'Actualizar lista de usuario'}
        />
      </Box>
      <Box style={{ position: 'relative', width: '100%', height: '85vh' }}>
        <svg
          width="100%"
          height="100%"
          style={{ border: '1px solid black', ...cursorStyle }} // Aplicar estilo de cursor al SVG
          onClick={handleSVGClick}
        >
          {renderGrid(window.innerWidth, window.innerHeight)}{' '}
          {/* Asegúrate de que estas dimensiones coincidan con el tamaño del SVG */}
          {standsData.map((stand) => {
            const area = parseFloat(stand.area)

            return (
              <g key={stand.id}>
                <rect
                  x={stand.ubicacionX - Math.sqrt(area) * 5}
                  y={stand.ubicacionY - Math.sqrt(area) * 5}
                  width={Math.sqrt(area) * 10}
                  height={Math.sqrt(area) * 10}
                  fill={stand.color}
                  stroke="black"
                  name="stand"
                  strokeWidth={2}
                  onMouseEnter={handleMouseEnter} // Cambiar cursor al entrar
                  onMouseLeave={handleMouseLeave} // Volver cursor al salir
                />
                <text
                  x={stand.ubicacionX}
                  y={stand.ubicacionY}
                  fill={theme.palette.text.primary}
                  name="stand"
                  fontSize="12"
                  textAnchor="middle"
                  dominantBaseline="middle"
                >
                  {stand.codigo} ({stand.area} m²)
                </text>
              </g>
            )
          })}
        </svg>
      </Box>

      <CustomDialog
        isOpen={modalStand}
        handleClose={cerrarModal}
        title={isEditing ? 'Editar Estánd' : 'Nuevo Estánd'}
        maxWidth="lg"
      >
        <VistaModalStand
          accionCorrecta={() => {
            cerrarModal()
            fetchStands().finally()
          }}
          accionCancelar={cerrarModal}
          ubicacionX={ubicacionX}
          ubicacionY={ubicacionY}
          stands={selectedStand}
        />
      </CustomDialog>
    </>
  )
}

export default StandsCustom
