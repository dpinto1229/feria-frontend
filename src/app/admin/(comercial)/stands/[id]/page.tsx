/* eslint-disable @next/next/no-img-element */
'use client'

import { useAlerts, useSession } from '@/hooks'
import { useEffect, useState } from 'react'
import { BloqueType } from '../types/StandsType'
import { Constantes } from '@/config/Constantes'
import { InterpreteMensajes, siteName } from '@/utils'
import {
  Box,
  Breadcrumbs,
  CardActionArea,
  CardContent,
  Grid,
  Link,
  Typography,
  useTheme,
} from '@mui/material'
import { IconoTooltip } from '@/components/botones/IconoTooltip'
import { useRouter } from 'next/navigation'

export default function DetalleBloque({ params }: { params: { id: string } }) {
  const id = params.id
  const { sesionPeticion } = useSession()
  const { Alerta } = useAlerts()
  const theme = useTheme()
  const router = useRouter()
  const [detalleData, setDetalleData] = useState<BloqueType[]>([])

  const fetchDatos = async () => {
    try {
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/stands/bloques/${id}`,
      })
      setDetalleData(respuesta?.datos)
    } catch (error) {
      Alerta({
        mensaje: `${InterpreteMensajes(error)}`,
        variant: 'error',
      })
    }
  }

  useEffect(() => {
    fetchDatos()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  function handleClick(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) {
    event.preventDefault()
    router.push('/admin/stands')
  }
  const breadcrumbs = [
    <Link
      underline="hover"
      key="2"
      color="inherit"
      href="/admin/stands"
      onClick={handleClick}
    >
      Bloques
    </Link>,
    <Typography key="3" sx={{ color: 'text.primary' }}>
      Plantas de stands
    </Typography>,
  ]
  return (
    <>
      <title>{`Gestión de plantas - ${siteName()}`}</title>

      <Typography variant="h6" gutterBottom>
        Gestión de Plantas
      </Typography>
      <Breadcrumbs aria-label="breadcrumb" separator="›">
        {breadcrumbs}
      </Breadcrumbs>
      <Box
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
        }}
      >
        <IconoTooltip
          id={'actualizarUsuario'}
          titulo={'Actualizar'}
          key={`actualizarUsuario`}
          accion={async () => {
            await fetchDatos()
          }}
          icono={'refresh'}
          name={'Actualizar lista de usuario'}
        />
      </Box>

      <Grid container spacing={2}>
        {detalleData?.map((stand, index) => (
          <Grid
            key={index}
            item
            xs={12}
            sm={9}
            md={6}
            lg={6}
            style={{ display: 'flex' }}
          >
            {stand.plantas.map((planta, indexPlanta) => (
              <Box key={indexPlanta} sx={{ width: '100%' }}>
                <CardContent
                  sx={{
                    border: `1px solid ${theme.palette.divider}`,
                    borderRadius: 5,
                    m: 1,
                    '&:hover': {
                      transform: 'scale(1.03)',
                    },
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                  }}
                >
                  <CardActionArea
                    onClick={() =>
                      router.push(`/admin/stands/detalle-stand/${planta.id}`)
                    }
                  >
                    <Box key={index}>
                      <Box
                        display="flex"
                        flexDirection="column"
                        alignItems="center"
                        mb={1}
                      >
                        <img
                          src={
                            planta.imagen === null
                              ? 'https://cdn-icons-png.flaticon.com/512/2748/2748558.png'
                              : planta.imagen
                          }
                          alt={planta.nivel}
                          style={{
                            width: '100%',
                            height: '150px',
                            borderRadius: '5px',
                            objectFit: 'cover',
                          }}
                        />
                        <Box sx={{ height: '10px' }} />
                        <Box
                          mt={1}
                          textAlign="center"
                          overflow="auto"
                          maxHeight="100px"
                        >
                          <Typography
                            variant="body1"
                            sx={{ fontSize: '16px', fontWeight: 600 }}
                          >
                            {planta.nivel}
                          </Typography>
                          <Typography
                            variant="body2"
                            sx={{ fontSize: '14px', fontWeight: 400 }}
                          >
                            {stand.descripcion}
                          </Typography>
                        </Box>
                      </Box>
                    </Box>
                  </CardActionArea>
                </CardContent>
              </Box>
            ))}
          </Grid>
        ))}
      </Grid>
    </>
  )
}
