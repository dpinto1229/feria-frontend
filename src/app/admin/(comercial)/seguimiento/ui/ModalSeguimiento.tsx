import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { Box, Button, DialogActions, DialogContent, Grid } from '@mui/material'
import { useAlerts, useSession } from '@/hooks'
import { delay, InterpreteMensajes } from '@/utils'
import { Constantes } from '@/config/Constantes'
import { imprimir } from '@/utils/imprimir'
import { FormInputDropdown, FormInputText } from 'src/components/form'
import ProgresoLineal from '@/components/progreso/ProgresoLineal'
import { EmpresaCrudType } from '../../empresas/types/empresaCrudType'

export interface ModalSeguimientoType {
  seguimiento?: any | null
  accionCorrecta: () => void
  accionCancelar: () => void
}

export const VistaModalSeguimiento = ({
  seguimiento,
  accionCorrecta,
  accionCancelar,
}: ModalSeguimientoType) => {
  const [loadingModal, setLoadingModal] = useState<boolean>(false)
  const [empresas, setEmpresas] = useState<EmpresaCrudType[]>([])
  // Hook para mostrar alertas
  const { Alerta } = useAlerts()

  // Proveedor de la sesión
  const { sesionPeticion } = useSession()

  const { handleSubmit, control } = useForm<any>({
    defaultValues: {
      id: seguimiento?.id,
      tipo: seguimiento?.tipo,
      medio: seguimiento?.medio,
      archivado: seguimiento?.archivado,
      observaciones: seguimiento?.observaciones,
      estadoEmpresa: seguimiento?.estadoEmpresa,
      idEmpresa: seguimiento?.empresa.id,
    },
  })

  const guadarSeguimiento = async (data: any) => {
    await guardarPeticion(data)
  }

  const guardarPeticion = async (seguimiento: any) => {
    try {
      setLoadingModal(true)
      await delay(1000)
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/seguimiento${
          seguimiento.id ? `/${seguimiento.id}` : ''
        }`,
        tipo: !!seguimiento.id ? 'patch' : 'post',
        body: seguimiento,
      })
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: 'success',
      })
      accionCorrecta()
    } catch (e) {
      imprimir(`Error al crear o actualizar parámetro`, e)
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: 'error' })
    } finally {
      setLoadingModal(false)
    }
  }
  const buscarEmpresa = async () => {
    setLoadingModal(true)
    try {
      await delay(1000)
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/empresas`,
        tipo: 'get',
      })
      setEmpresas(respuesta.datos.filas)
    } catch (e) {
      imprimir(`Error al buscar empresa`, e)
    } finally {
      setLoadingModal(false)
    }
  }

  useEffect(() => {
    buscarEmpresa()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <form onSubmit={handleSubmit(guadarSeguimiento)}>
      <DialogContent dividers>
        <Grid container direction={'column'} justifyContent="space-evenly">
          {seguimiento?.id ? (
            <Grid container direction="row" spacing={{ xs: 2, sm: 1, md: 2 }}>
              <Grid item xs={12} sm={12} md={12}>
                <FormInputText
                  id={'estadoEmpresa'}
                  control={control}
                  name="estadoEmpresa"
                  label="Estado empresa"
                  disabled={loadingModal}
                  rules={{ required: 'Este campo es requerido' }}
                />
              </Grid>
            </Grid>
          ) : (
            <>
              <Grid container direction="row" spacing={{ xs: 2, sm: 1, md: 2 }}>
                <Grid item xs={12} sm={12} md={6}>
                  <FormInputText
                    id={'tipo'}
                    control={control}
                    name="tipo"
                    label="Tipo"
                    disabled={loadingModal}
                    rules={{ required: 'Este campo es requerido' }}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={6}>
                  <FormInputText
                    id={'medio'}
                    control={control}
                    name="medio"
                    label="Medio"
                    disabled={loadingModal}
                    rules={{ required: 'Este campo es requerido' }}
                  />
                </Grid>
              </Grid>
              <Box height={'15px'} />
              <Grid container direction="row" spacing={{ xs: 2, sm: 1, md: 2 }}>
                <Grid item xs={12} sm={12} md={6}>
                  <FormInputText
                    id={'archivado'}
                    control={control}
                    name="archivado"
                    label="Archivado"
                    disabled={loadingModal}
                    rules={{ required: 'Este campo es requerido' }}
                  />
                </Grid>

                <Grid item xs={12} sm={12} md={6}>
                  <FormInputDropdown
                    id="idEmpresa"
                    control={control}
                    name="idEmpresa"
                    label="Empresa"
                    disabled={loadingModal}
                    rules={{ required: 'Este campo es requerido' }}
                    options={empresas?.map((empresa) => ({
                      key: empresa.id || '',
                      label: empresa.nombreEmpresa || '',
                      value: empresa.id || '',
                    }))}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <FormInputText
                    id={'observaciones'}
                    control={control}
                    name="observaciones"
                    label="Observaciones"
                    rows={5}
                    multiline
                    disabled={loadingModal}
                    rules={{ required: 'Este campo es requerido' }}
                  />
                </Grid>
              </Grid>
            </>
          )}
          <Box height={'10px'} />
          <ProgresoLineal mostrar={loadingModal} />
          <Box height={'5px'} />
        </Grid>
      </DialogContent>
      <DialogActions
        sx={{
          my: 1,
          mx: 2,
          justifyContent: {
            lg: 'flex-end',
            md: 'flex-end',
            xs: 'center',
            sm: 'center',
          },
        }}
      >
        <Button
          variant={'outlined'}
          disabled={loadingModal}
          onClick={accionCancelar}
        >
          Cancelar
        </Button>
        <Button variant={'contained'} disabled={loadingModal} type={'submit'}>
          Guardar
        </Button>
      </DialogActions>
    </form>
  )
}
