import * as React from 'react'
import Timeline from '@mui/lab/Timeline'
import TimelineItem from '@mui/lab/TimelineItem'
import TimelineSeparator from '@mui/lab/TimelineSeparator'
import TimelineConnector from '@mui/lab/TimelineConnector'
import TimelineContent from '@mui/lab/TimelineContent'
import TimelineDot from '@mui/lab/TimelineDot'
import TimelineOppositeContent from '@mui/lab/TimelineOppositeContent'
import { Button, DialogActions, DialogContent, Typography } from '@mui/material'
import dayjs from 'dayjs'

interface Props {
  accionCancelar: () => void
  data: any
}
export default function HistorialComponent({ accionCancelar, data }: Props) {
  return (
    <>
      <DialogContent dividers>
        <Timeline position="alternate">
          <TimelineItem>
            <TimelineOppositeContent color="text.secondary">
              {dayjs(data.fechaCreacion).format('DD-MM-YYYY HH:mm')}
            </TimelineOppositeContent>
            <TimelineSeparator>
              <TimelineDot />
              <TimelineConnector />
            </TimelineSeparator>
            <TimelineContent>
              <Typography
                sx={{
                  fontSize: '16px',
                  fontWeight: 500,
                }}
              >
                Tipo:
              </Typography>
              <Typography>{data.tipo}</Typography>
              <Typography
                sx={{
                  fontSize: '16px',
                  fontWeight: 500,
                }}
              >
                Usuario Creacion:
              </Typography>
              <Typography>{data.empresa.nombreEncargado}</Typography>
            </TimelineContent>
          </TimelineItem>
        </Timeline>
      </DialogContent>
      <DialogActions
        sx={{
          my: 1,
          mx: 2,
          justifyContent: {
            lg: 'flex-end',
            md: 'flex-end',
            xs: 'center',
            sm: 'center',
          },
        }}
      >
        <Button variant={'outlined'} onClick={accionCancelar}>
          Cancelar
        </Button>
      </DialogActions>
    </>
  )
}
