import { Box, Grid } from '@mui/material'

import { useForm } from 'react-hook-form'
import { useDebouncedCallback } from 'use-debounce'
import { useEffect } from 'react'
import { FormInputText } from '@/components/form/FormInputText'
import { FormInputDropdown } from '@/components/form'

export interface FiltroType {
  tipo: string
}

export interface FiltroParametrosType {
  filtroParametro: string
  accionCorrecta: (filtros: FiltroType) => void
  accionCerrar: () => void
}

export const FiltroIngreso = ({
  filtroParametro,
  accionCorrecta,
}: FiltroParametrosType) => {
  const { control, watch } = useForm<FiltroType>({
    defaultValues: {
      tipo: filtroParametro,
    },
  })

  const parametroFiltro: string | undefined = watch('tipo')

  useEffect(() => {
    actualizacionFiltros({
      tipo: parametroFiltro,
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [parametroFiltro])

  const debounced = useDebouncedCallback((filtros: FiltroType) => {
    accionCorrecta(filtros)
  }, 1000)

  const actualizacionFiltros = (filtros: FiltroType) => {
    debounced(filtros)
  }
  const tipoIngreso = [
    { value: 'INGRESO', label: 'Ingreso' },
    { value: 'EGRESO', label: 'Egreso' },
  ]
  return (
    <Box sx={{ pl: 1, pr: 1, pt: 1 }}>
      <Grid container direction="row" spacing={{ xs: 2, sm: 1, md: 2 }}>
        <Grid item xs={12} sm={12} md={6}>
          <FormInputDropdown
            id={'tipo'}
            control={control}
            name="tipo"
            label="Tipo"
            rules={{ required: 'Este campo es requerido' }}
            options={tipoIngreso.map((item) => ({
              key: item.value,
              value: item.value,
              label: item.label,
            }))}
          />
        </Grid>
      </Grid>
    </Box>
  )
}
