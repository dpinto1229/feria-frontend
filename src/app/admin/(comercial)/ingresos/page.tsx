'use client'
import Typography from '@mui/material/Typography'
import { ReactNode, useEffect, useState } from 'react'
import { useAlerts, useSession } from '@/hooks'
import { useAuth } from '@/context/AuthProvider'
import { CasbinTypes } from '@/types'
import { Button, Card, Grid, useMediaQuery, useTheme } from '@mui/material'
import { delay, InterpreteMensajes, siteName, titleCase } from '@/utils'
import { Constantes } from '@/config/Constantes'
import { imprimir } from '@/utils/imprimir'
import { usePathname } from 'next/navigation'
import { CriterioOrdenType } from '@/types/ordenTypes'
import CustomMensajeEstado from '@/components/estados/CustomMensajeEstado'
import { IconoTooltip } from '@/components/botones/IconoTooltip'
import { BotonBuscar } from '@/components/botones/BotonBuscar'
import { BotonOrdenar } from '@/components/botones/BotonOrdenar'
import { IconoBoton } from '@/components/botones/IconoBoton'
import { ordenFiltrado } from '@/utils/orden'
import { Paginacion } from '@/components/datatable/Paginacion'
import { AlertDialog } from '@/components/modales/AlertDialog'
import { CustomDialog } from '@/components/modales/CustomDialog'
import { CustomDataTable } from '@/components/datatable/CustomDataTable'
import { FiltroParametros } from '@/app/admin/(configuracion)/parametros/ui/FiltroParametros'
import { VistaModalIngreso } from './ui/ModalIngresos'
import dayjs from 'dayjs'
import { FiltroIngreso } from './ui/FIltroIngresos'
import jsPDF from 'jspdf'
import 'jspdf-autotable'
import { Icono } from '@/components/Icono'

declare module 'jspdf' {
  interface jsPDF {
    autoTable: (options: {
      head: Array<Array<string | number>>
      body: Array<Array<string | number>>
    }) => jsPDF
  }
}
export default function IngresosPage() {
  const [parametrosData, setParametrosData] = useState<any[]>([])
  const [loading, setLoading] = useState<boolean>(true)

  // Hook para mostrar alertas
  const { Alerta } = useAlerts()
  const [errorParametrosData, setErrorParametrosData] = useState<any>()

  const [modalParametro, setModalParametro] = useState(false)

  /// Indicador para mostrar una vista de alerta de cambio de estado
  const [mostrarAlertaEstadoParametro, setMostrarAlertaEstadoParametro] =
    useState(false)

  const [parametroEdicion, setParametroEdicion] = useState<
    any | undefined | null
  >()

  // Variables de páginado
  const [limite, setLimite] = useState<number>(10)
  const [pagina, setPagina] = useState<number>(1)
  const [total, setTotal] = useState<number>(0)

  const { sesionPeticion } = useSession()
  const { permisoUsuario } = useAuth()

  const [presupuesto, setPresupuesto] = useState<any>()
  const [filtroParametro, setFiltroParametro] = useState<string>('')
  const [mostrarFiltroParametros, setMostrarFiltroParametros] = useState(false)
  // Permisos para acciones
  const [permisos, setPermisos] = useState<CasbinTypes>({
    read: false,
    create: false,
    update: false,
    delete: false,
  })

  const theme = useTheme()
  const xs = useMediaQuery(theme.breakpoints.only('xs'))

  /// Método que muestra alerta de cambio de estado

  const editarEstadoParametroModal = async (parametro: any) => {
    setParametroEdicion(parametro) // para mostrar datos de modal en la alerta
    setMostrarAlertaEstadoParametro(true) // para mostrar alerta de parametro
  }

  const cancelarAlertaEstadoParametro = async () => {
    setMostrarAlertaEstadoParametro(false)
    await delay(500) // para no mostrar undefined mientras el modal se cierra
    setParametroEdicion(null)
  }

  /// Método que oculta la alerta de cambio de estado y procede
  const aceptarAlertaEstadoParametro = async () => {
    setMostrarAlertaEstadoParametro(false)
    if (parametroEdicion) {
      await cambiarEstadoParametroPeticion(parametroEdicion)
    }
    setParametroEdicion(null)
  }

  /// Petición que cambia el estado de un parámetro
  const cambiarEstadoParametroPeticion = async (parametro: any) => {
    try {
      setLoading(true)
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/ingresos/${parametro.id}/${
          parametro.estado == 'ACTIVO' ? 'inactivacion' : 'activacion'
        }`,
        tipo: 'patch',
      })
      imprimir(`respuesta estado parametro: ${respuesta}`)
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: 'success',
      })
      await obtenerParametrosPeticion()
      await fetchPresupuesto()
    } catch (e) {
      imprimir(`Error estado parametro`, e)
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: 'error' })
    } finally {
      setLoading(false)
    }
  }

  const fetchPresupuesto = async () => {
    try {
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/ingresos/presupuesto`,
      })
      setPresupuesto(respuesta.datos.total)
    } catch (e) {
      imprimir(`Error al obtener presupuesto`, e)
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: 'error' })
    }
  }

  // router para conocer la ruta actual
  const pathname = usePathname()

  /// Criterios de orden
  const [ordenCriterios, setOrdenCriterios] = useState<
    Array<CriterioOrdenType>
  >([
    { campo: 'codigo', nombre: 'Descripcion', ordenar: true },
    { campo: 'nombre', nombre: 'Tipo', ordenar: true },
    { campo: 'descripcion', nombre: 'Monto', ordenar: true },
    { campo: 'grupo', nombre: 'Fecha', ordenar: true },
    { campo: 'acciones', nombre: 'Acciones' },
  ])

  const contenidoTabla: Array<Array<ReactNode>> = parametrosData.map(
    (parametroData, indexParametro) => [
      <Typography
        key={`${parametroData.id}-${indexParametro}-codigo`}
        variant={'body2'}
      >{`${parametroData.descripcion}`}</Typography>,
      <CustomMensajeEstado
        key={`${parametroData.id}-${indexParametro}-estado`}
        titulo={parametroData.tipo}
        descripcion={parametroData.tipo}
        color={
          parametroData.tipo == 'INGRESO'
            ? 'info'
            : parametroData.tipo == 'EGRESO'
              ? 'error'
              : 'info'
        }
      />,
      <Typography
        key={`${parametroData.id}-${indexParametro}-descripcion`}
        variant={'body2'}
      >{`${parametroData.monto}`}</Typography>,
      <Typography
        key={`${parametroData.id}-${indexParametro}-grupo`}
        variant={'body2'}
      >{`${dayjs(parametroData.fechaCreacion).format(
        'DD/MM/YY HH:mm'
      )}`}</Typography>,

      <Grid key={`${parametroData.id}-${indexParametro}-acciones`}>
        {/* {permisos.update && (
          <IconoTooltip
            id={`cambiarEstadoParametro-${parametroData.id}`}
            titulo={parametroData.estado == 'ACTIVO' ? 'Inactivar' : 'Activar'}
            color={parametroData.estado == 'ACTIVO' ? 'success' : 'error'}
            accion={async () => {
              await editarEstadoParametroModal(parametroData)
            }}
            desactivado={parametroData.estado == 'PENDIENTE'}
            icono={
              parametroData.estado == 'ACTIVO' ? 'toggle_on' : 'toggle_off'
            }
            name={
              parametroData.estado == 'ACTIVO'
                ? 'Inactivar Parámetro'
                : 'Activar Parámetro'
            }
          />
        )} */}

        {permisos.update && (
          <IconoTooltip
            id={`editarParametros-${parametroData.id}`}
            name={'Parámetros'}
            titulo={'Editar'}
            color={'primary'}
            accion={() => {
              imprimir(`Editaremos`, parametroData)
              editarParametroModal(parametroData)
            }}
            icono={'edit'}
          />
        )}
      </Grid>,
    ]
  )
  const generarPDF = () => {
    const doc = new jsPDF()
    doc.text('Reportes de ingreos', 15, 10)

    const rows = parametrosData.map((parametroData) => [
      `${parametroData.descripcion}`,

      //hacemos que tenga un color dependiendo al estado
      `${parametroData.tipo}`,
      //roles
      `${parametroData.monto}`,
      `${dayjs(parametroData.fechaCreacion).format('DD/MM/YY HH:mm')}`,
    ])

    doc.autoTable({
      head: [['Descipcion', 'Tipo', 'Monto', 'Fecha']],
      body: rows,
    })

    doc.save('reporte-ingresos.pdf')
  }

  const acciones: Array<ReactNode> = [
    <Card
      key={'presupuesto'}
      sx={{
        padding: '10px',
        margin: '5px',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        border: `1px solid ${theme.palette.primary.main}`,
        borderRadius: '10px',
        //hacemos que se levante el card al pasar el mouse
        transition: 'transform 0.3s',
        '&:hover': {
          transform: 'scale(1.05)',
        },
        //hacemos que el card tenga el fondo dependiendo al presupeusto
        backgroundColor:
          presupuesto && presupuesto > 0
            ? `${theme.palette.success.main}`
            : `${theme.palette.error.main}`,
      }}
    >
      <Typography variant={'body2'}>Presupuesto</Typography>
      <Typography variant={'body1'}>{`${
        presupuesto ? presupuesto : '0.00'
      } Bs`}</Typography>
    </Card>,
     <Button
     key="descargarPdf"
     sx={{ marginLeft: '5px', borderRadius: 5 }}
     startIcon={<Icono color="inherit">get_app</Icono>}
     onClick={() => {
       generarPDF()
     }}
     variant="contained"
     color="primary"
   >
     Descargar PDF
   </Button>,
    <BotonBuscar
      id={'accionFiltrarParametrosToggle'}
      key={'accionFiltrarParametrosToggle'}
      seleccionado={mostrarFiltroParametros}
      cambiar={setMostrarFiltroParametros}
    />,
    xs && (
      <BotonOrdenar
        id={'ordenarParametros'}
        key={`ordenarParametros`}
        label={'Ordenar parámetros'}
        criterios={ordenCriterios}
        cambioCriterios={setOrdenCriterios}
      />
    ),
    <IconoTooltip
      id={'actualizarParametro'}
      titulo={'Actualizar'}
      key={`accionActualizarParametro`}
      accion={async () => {
        await obtenerParametrosPeticion()
        await fetchPresupuesto()
      }}
      icono={'refresh'}
      name={'Actualizar lista de parámetros'}
    />,
    permisos.create && (
      <IconoBoton
        id={'agregarParametro'}
        key={'agregarParametro'}
        texto={'Agregar'}
        variante={xs ? 'icono' : 'boton'}
        icono={'add_circle_outline'}
        descripcion={'Agregar parámetro'}
        accion={() => {
          agregarParametroModal()
        }}
      />
    ),
   
  ]

  const obtenerParametrosPeticion = async () => {
    try {
      setLoading(true)

      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/ingresos`,
        params: {
          pagina: pagina,
          limite: limite,
          ...(filtroParametro.length == 0 ? {} : { filtro: filtroParametro }),
          ...(ordenFiltrado(ordenCriterios).length == 0
            ? {}
            : {
                orden: ordenFiltrado(ordenCriterios).join(','),
              }),
        },
      })
      setParametrosData(respuesta.datos?.filas)
      setTotal(respuesta.datos?.total)
      setErrorParametrosData(null)
    } catch (e) {
      imprimir(`Error al obtener parametros`, e)
      setErrorParametrosData(e)
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: 'error' })
    } finally {
      setLoading(false)
    }
  }

  const agregarParametroModal = () => {
    setParametroEdicion(undefined)
    setModalParametro(true)
  }
  const editarParametroModal = (parametro: any) => {
    setParametroEdicion(parametro)
    setModalParametro(true)
  }

  const cerrarModalParametro = async () => {
    setModalParametro(false)
    await delay(500)
    setParametroEdicion(undefined)
  }

  async function definirPermisos() {
    setPermisos(await permisoUsuario(pathname))
  }

  useEffect(() => {
    definirPermisos().finally()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    obtenerParametrosPeticion().finally(() => {})
    fetchPresupuesto().finally(() => {})
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    pagina,
    limite,
    // eslint-disable-next-line react-hooks/exhaustive-deps
    JSON.stringify(ordenCriterios),
    filtroParametro,
  ])

  useEffect(() => {
    if (!mostrarFiltroParametros) {
      setFiltroParametro('')
    }
  }, [mostrarFiltroParametros])

  const paginacion = (
    <Paginacion
      pagina={pagina}
      limite={limite}
      total={total}
      cambioPagina={setPagina}
      cambioLimite={setLimite}
    />
  )

  return (
    <>
      <title>{`Ingresos - ${siteName()}`}</title>
      <AlertDialog
        isOpen={mostrarAlertaEstadoParametro}
        titulo={'Alerta'}
        texto={`¿Está seguro de ${
          parametroEdicion?.estado == 'ACTIVO' ? 'inactivar' : 'activar'
        } el parámetro: ${titleCase(parametroEdicion?.nombre ?? '')} ?`}
      >
        <Button onClick={cancelarAlertaEstadoParametro}>Cancelar</Button>
        <Button onClick={aceptarAlertaEstadoParametro}>Aceptar</Button>
      </AlertDialog>
      <CustomDialog
        isOpen={modalParametro}
        handleClose={cerrarModalParametro}
        title={parametroEdicion ? 'Editar parámetro' : 'Nuevo parámetro'}
      >
        <VistaModalIngreso
          parametro={parametroEdicion}
          accionCorrecta={() => {
            cerrarModalParametro().finally()
            obtenerParametrosPeticion().finally()
            fetchPresupuesto().finally()
          }}
          accionCancelar={cerrarModalParametro}
        />
      </CustomDialog>
      <CustomDataTable
        titulo={'Ingresos'}
        error={!!errorParametrosData}
        cargando={loading}
        acciones={acciones}
        columnas={ordenCriterios}
        cambioOrdenCriterios={setOrdenCriterios}
        paginacion={paginacion}
        contenidoTabla={contenidoTabla}
        filtros={
          mostrarFiltroParametros && (
            <FiltroIngreso
              filtroParametro={filtroParametro}
              accionCorrecta={(filtros) => {
                setPagina(1)
                setLimite(10)
                setFiltroParametro(filtros.tipo)
              }}
              accionCerrar={() => {
                imprimir(`👀 cerrar`)
              }}
            />
          )
        }
      />
    </>
  )
}
