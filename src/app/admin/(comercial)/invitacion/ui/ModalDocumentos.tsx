import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { Box, Button, DialogActions, DialogContent, Grid } from '@mui/material'
import { useAlerts, useSession } from '@/hooks'
import { delay, InterpreteMensajes } from '@/utils'
import { Constantes } from '@/config/Constantes'
import { imprimir } from '@/utils/imprimir'
import { FormInputDropdown, FormInputText } from 'src/components/form'
import ProgresoLineal from '@/components/progreso/ProgresoLineal'
import FormInputFile from '@/components/form/FormInputFile'

export interface ModalParametroType {
  parametro?: any | null
  accionCorrecta: () => void
  accionCancelar: () => void
}

export const VistaModalDocumentos = ({
  parametro,
  accionCorrecta,
  accionCancelar,
}: ModalParametroType) => {
  const [loadingModal, setLoadingModal] = useState<boolean>(false)
  const [entidadData, setEntidadData] = useState<any[]>([])
  // Hook para mostrar alertas
  const { Alerta } = useAlerts()

  // Proveedor de la sesión
  const { sesionPeticion } = useSession()

  const { handleSubmit, control } = useForm<any>({
    defaultValues: {
      id: parametro?.id,
      documentoInvitacion: parametro?.documentoInvitacion,
      idEmpresa: parametro?.empresa?.id,
    },
  })

  const guardarActualizarParametro = async (data: any) => {
    await guardarActualizarParametrosPeticion(data)
  }

  const guardarActualizarParametrosPeticion = async (parametro: any) => {
    try {
      setLoadingModal(true)
      await delay(1000)

      const formData = new FormData()
      formData.append('file', parametro.documentoInvitacion[0])
      formData.append('idEmpresa', parametro.idEmpresa)
      formData.append('descripcion', parametro.descripcion)
      formData.append('tipoDocumento', 'INVITACION')
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/documento${
          parametro.id ? `/${parametro.id}` : ''
        }`,
        tipo: !!parametro.id ? 'patch' : 'post',
        body: formData,
      })
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: 'success',
      })
      accionCorrecta()
    } catch (e) {
      imprimir(`Error al crear o actualizar parámetro`, e)
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: 'error' })
    } finally {
      setLoadingModal(false)
    }
  }

  const obtenerEntidades = async () => {
    try {
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/empresas`,
        tipo: 'get',
      })
      setEntidadData(respuesta.datos.filas)
    } catch (e) {
      imprimir(`Error al obtener entidades`, e)
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: 'error' })
    }
  }

  useEffect(() => {
    obtenerEntidades()
  }, [])

  return (
    <form onSubmit={handleSubmit(guardarActualizarParametro)}>
      <DialogContent dividers>
        <Grid container direction={'column'} justifyContent="space-evenly">
          <Grid container direction="row" spacing={{ xs: 2, sm: 1, md: 2 }}>
          <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={'descripcion'}
                control={control}
                name="descripcion"
                label="Descripción"
                disabled={loadingModal}
                rows={2}
                multiline
                rules={{ required: 'Este campo es requerido' }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputFile
                id={'documentoInvitacion'}
                control={control}
                name="documentoInvitacion"
                label="Documento de invitacion"
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputDropdown
                id={'idEmpresa'}
                control={control}
                name="idEmpresa"
                label="Empresas"
                disabled={loadingModal}
                options={entidadData.map((entidad: any) => ({
                  value: entidad.id,
                  key: entidad.id,
                  label: entidad.nombreEmpresa,
                }))}
                rules={{ required: 'Este campo es requerido' }}
              />
            </Grid>
          </Grid>
          <Box height={'15px'} />
          <ProgresoLineal mostrar={loadingModal} />
          <Box height={'5px'} />
        </Grid>
      </DialogContent>
      <DialogActions
        sx={{
          my: 1,
          mx: 2,
          justifyContent: {
            lg: 'flex-end',
            md: 'flex-end',
            xs: 'center',
            sm: 'center',
          },
        }}
      >
        <Button
          variant={'outlined'}
          disabled={loadingModal}
          onClick={accionCancelar}
        >
          Cancelar
        </Button>
        <Button variant={'contained'} disabled={loadingModal} type={'submit'}>
          Guardar
        </Button>
      </DialogActions>
    </form>
  )
}
