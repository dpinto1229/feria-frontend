import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { Box, Button, DialogActions, DialogContent, Grid } from '@mui/material'
import { useAlerts, useSession } from '@/hooks'
import { delay, InterpreteMensajes } from '@/utils'
import { Constantes } from '@/config/Constantes'
import { imprimir } from '@/utils/imprimir'
import { FormInputText } from 'src/components/form'
import ProgresoLineal from '@/components/progreso/ProgresoLineal'
import { EmpresaCrudType } from '../types/empresaCrudType'

export interface ModalParametroType {
  parametro?: EmpresaCrudType | null
  accionCorrecta: () => void
  accionCancelar: () => void
}

export const VistaModalEmpresas = ({
  parametro,
  accionCorrecta,
  accionCancelar,
}: ModalParametroType) => {
  const [loadingModal, setLoadingModal] = useState<boolean>(false)

  // Hook para mostrar alertas
  const { Alerta } = useAlerts()

  // Proveedor de la sesión
  const { sesionPeticion } = useSession()

  const { handleSubmit, control } = useForm<EmpresaCrudType>({
    defaultValues: {
      id: parametro?.id,
      nombreEmpresa: parametro?.nombreEmpresa,
      rubroEmpresa: parametro?.rubroEmpresa,
      descripcionEmpresa: parametro?.descripcionEmpresa,
      correoEmpresa: parametro?.correoEmpresa,
      telefonoEmpresa: parametro?.telefonoEmpresa,
      nombreEncargado: parametro?.nombreEncargado,
      direccionEmpresa: parametro?.direccionEmpresa,
      nitEmpresa: parametro?.nitEmpresa,
      evento: parametro?.evento,
      
    },
  })

  const guardarActualizarParametro = async (
    data: EmpresaCrudType
  ) => {
    await guardarActualizarParametrosPeticion(data)
  }

  const guardarActualizarParametrosPeticion = async (
    parametro: EmpresaCrudType
  ) => {
    try {
      setLoadingModal(true)
      await delay(1000)
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/empresas${
          parametro.id ? `/${parametro.id}` : ''
        }`,
        tipo: !!parametro.id ? 'patch' : 'post',
        body: parametro,
      })
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: 'success',
      })
      accionCorrecta()
    } catch (e) {
      imprimir(`Error al crear o actualizar parámetro`, e)
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: 'error' })
    } finally {
      setLoadingModal(false)
    }
  }

  return (
    <form onSubmit={handleSubmit(guardarActualizarParametro)}>
      <DialogContent dividers>
        <Grid container direction={'column'} justifyContent="space-evenly">
          <Grid container direction="row" spacing={{ xs: 2, sm: 1, md: 2 }}>
            <Grid item xs={12} sm={12} md={6}>
              <FormInputText
                id={'nombreEmpresa'}
                control={control}
                name="nombreEmpresa"
                label="Nombre"
                disabled={loadingModal}
                rules={{ required: 'Este campo es requerido' }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={6}>
              <FormInputText
                id={'rubroEmpresa'}
                control={control}
                name="rubroEmpresa"
                label="Rubro"
                disabled={loadingModal}
                rules={{ required: 'Este campo es requerido' }}
              />
            </Grid>
          </Grid>
          <Box height={'15px'} />
          <Grid container direction="row" spacing={{ xs: 2, sm: 1, md: 2 }}>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={'descripcionEmpresa'}
                control={control}
                name="descripcionEmpresa"
                label="Descripción"
                multiline
                rows={4}
                disabled={loadingModal}
                rules={{ required: 'Este campo es requerido' }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={6}>
              <FormInputText
                id={'correoEmpresa'}
                control={control}
                name="correoEmpresa"
                label="Correo"
                type='email'
                disabled={loadingModal}
                rules={{ required: 'Este campo es requerido' }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={6}>
              <FormInputText
                id={'telefonoEmpresa'}
                control={control}
                name="telefonoEmpresa"
                label="Telefono"
                type='number'
                disabled={loadingModal}
                rules={{ required: 'Este campo es requerido' }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={6}>
              <FormInputText
                id={'nombreEncargado'}
                control={control}
                name="nombreEncargado"
                label="Encargado"
                disabled={loadingModal}
                rules={{ required: 'Este campo es requerido' }}
              />
            </Grid>
     
            <Grid item xs={12} sm={12} md={6}>
              <FormInputText
                id={'nitEmpresa'}
                control={control}
                name="nitEmpresa"
                type='number'
                label="NIT de la empresa"
                disabled={loadingModal}
                rules={{ required: 'Este campo es requerido' }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={'direccionEmpresa'}
                control={control}
                name="direccionEmpresa"
                multiline
                rows={4}
                label="Direcccion de la empresa"
                disabled={loadingModal}
                rules={{ required: 'Este campo es requerido' }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={'evento'}
                control={control}
                name="evento"
                multiline
                label="Evento"
                disabled={loadingModal}
                rules={{ required: 'Este campo es requerido' }}
              />
            </Grid>
          </Grid>
          <Box height={'10px'} />
          <ProgresoLineal mostrar={loadingModal} />
          <Box height={'5px'} />
        </Grid>
      </DialogContent>
      <DialogActions
        sx={{
          my: 1,
          mx: 2,
          justifyContent: {
            lg: 'flex-end',
            md: 'flex-end',
            xs: 'center',
            sm: 'center',
          },
        }}
      >
        <Button
          variant={'outlined'}
          disabled={loadingModal}
          onClick={accionCancelar}
        >
          Cancelar
        </Button>
        <Button variant={'contained'} disabled={loadingModal} type={'submit'}>
          Guardar
        </Button>
      </DialogActions>
    </form>
  )
}
