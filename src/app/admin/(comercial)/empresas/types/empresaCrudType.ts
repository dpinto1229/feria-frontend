export interface EmpresaCrudType {
id?: string

nombreEmpresa: string

rubroEmpresa: string

telefonoEmpresa: number

correoEmpresa: string

descripcionEmpresa: string

nombreEncargado: string

direccionEmpresa: string

nitEmpresa: string

evento: string

estado?: string
}