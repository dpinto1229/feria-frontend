'use client'
import Typography from '@mui/material/Typography'
import { ReactNode, useEffect, useState } from 'react'
import { useAlerts, useSession } from '@/hooks'
import { useAuth } from '@/context/AuthProvider'
import { CasbinTypes } from '@/types'
import { Button, Grid, useMediaQuery, useTheme } from '@mui/material'
import { delay, InterpreteMensajes, siteName, titleCase } from '@/utils'
import { Constantes } from '@/config/Constantes'
import { imprimir } from '@/utils/imprimir'
import { usePathname } from 'next/navigation'
import { CriterioOrdenType } from '@/types/ordenTypes'
import CustomMensajeEstado from '@/components/estados/CustomMensajeEstado'
import { IconoTooltip } from '@/components/botones/IconoTooltip'
import { BotonBuscar } from '@/components/botones/BotonBuscar'
import { BotonOrdenar } from '@/components/botones/BotonOrdenar'
import { IconoBoton } from '@/components/botones/IconoBoton'
import { ordenFiltrado } from '@/utils/orden'
import { Paginacion } from '@/components/datatable/Paginacion'
import { AlertDialog } from '@/components/modales/AlertDialog'
import { CustomDialog } from '@/components/modales/CustomDialog'
import { CustomDataTable } from '@/components/datatable/CustomDataTable'
import { FiltroParametros } from '@/app/admin/(configuracion)/parametros/ui/FiltroParametros'
import { EmpresaCrudType } from './types/empresaCrudType'
import { VistaModalEmpresas } from './ui/ModalEmpresas'
import jsPDF from 'jspdf'
import 'jspdf-autotable'
import { Icono } from '@/components/Icono'
import { FiltroEmpresa } from './ui/FiltroEmpresas'

declare module 'jspdf' {
  interface jsPDF {
    autoTable: (options: {
      head: Array<Array<string | number>>
      body: Array<Array<string | number>>
    }) => jsPDF
  }
}
export default function ParametrosPage() {
  const [parametrosData, setParametrosData] = useState<EmpresaCrudType[]>([])
  const [loading, setLoading] = useState<boolean>(true)

  // Hook para mostrar alertas
  const { Alerta } = useAlerts()
  const [errorParametrosData, setErrorParametrosData] = useState<any>()

  const [modalParametro, setModalParametro] = useState(false)

  /// Indicador para mostrar una vista de alerta de cambio de estado
  const [mostrarAlertaEstadoParametro, setMostrarAlertaEstadoParametro] =
    useState(false)

  const [parametroEdicion, setParametroEdicion] = useState<
    EmpresaCrudType | undefined | null
  >()

  // Variables de páginado
  const [limite, setLimite] = useState<number>(10)
  const [pagina, setPagina] = useState<number>(1)
  const [total, setTotal] = useState<number>(0)

  const { sesionPeticion } = useSession()
  const { permisoUsuario } = useAuth()

  const [filtroParametro, setFiltroParametro] = useState<string>('')
  const [mostrarFiltroParametros, setMostrarFiltroParametros] = useState(false)
  // Permisos para acciones
  const [permisos, setPermisos] = useState<CasbinTypes>({
    read: false,
    create: false,
    update: false,
    delete: false,
  })

  const theme = useTheme()
  const xs = useMediaQuery(theme.breakpoints.only('xs'))

  /// Método que muestra alerta de cambio de estado

  const editarEstadoParametroModal = async (parametro: EmpresaCrudType) => {
    setParametroEdicion(parametro) // para mostrar datos de modal en la alerta
    setMostrarAlertaEstadoParametro(true) // para mostrar alerta de parametro
  }

  const cancelarAlertaEstadoParametro = async () => {
    setMostrarAlertaEstadoParametro(false)
    await delay(500) // para no mostrar undefined mientras el modal se cierra
    setParametroEdicion(null)
  }

  /// Método que oculta la alerta de cambio de estado y procede
  const aceptarAlertaEstadoParametro = async () => {
    setMostrarAlertaEstadoParametro(false)
    if (parametroEdicion) {
      await cambiarEstadoParametroPeticion(parametroEdicion)
    }
    setParametroEdicion(null)
  }

  /// Petición que cambia el estado de un parámetro
  const cambiarEstadoParametroPeticion = async (parametro: EmpresaCrudType) => {
    try {
      setLoading(true)
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/empresas/${parametro.id}/${
          parametro.estado == 'ACTIVO' ? 'inactivacion' : 'activacion'
        }`,
        tipo: 'patch',
      })
      imprimir(`respuesta estado parametro: ${respuesta}`)
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: 'success',
      })
      await obtenerParametrosPeticion()
    } catch (e) {
      imprimir(`Error estado parametro`, e)
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: 'error' })
    } finally {
      setLoading(false)
    }
  }

  const generarPDF = () => {
    const doc = new jsPDF()
    doc.text('Listado de empresas', 15, 10)

    const rows = parametrosData.map((parametroData) => [
      `${parametroData.nombreEmpresa}`,
      `${parametroData.rubroEmpresa}`,
      `${parametroData.telefonoEmpresa}`,
      `${parametroData.correoEmpresa}`,
      `${parametroData.descripcionEmpresa}`,
      `${parametroData.nombreEncargado}`,
      `${parametroData.direccionEmpresa}`,
      `${parametroData.nitEmpresa}`,
      `${parametroData.evento}`,
      `${parametroData.estado}`,
    ])

    doc.autoTable({
      head: [[
        'Nombre de la empresa',
        'Rubro',
        'Telefono',
        'Correo',
        'Descripción',
        'Nombre del encargado',
        'Dirección',
        'Nit de la empresa',
        'Evento',
        'Estado',
      ]],
      body: rows,
    })

    doc.save('listado-empresas.pdf')
  }

  // router para conocer la ruta actual
  const pathname = usePathname()

  /// Criterios de orden
  const [ordenCriterios, setOrdenCriterios] = useState<
    Array<CriterioOrdenType>
  >([
    { campo: 'nombreEmpresa', nombre: 'Nombre de la empresa', ordenar: true },
    { campo: 'rubroEmpresa', nombre: 'Rubro', ordenar: true },
    { campo: 'telefonoEmpresa', nombre: 'Telefono', ordenar: true },
    { campo: 'correoEmpresa', nombre: 'Correo', ordenar: true },
    { campo: 'descripcionEmpresa', nombre: 'Descripción', ordenar: true },
    { campo: 'nombreEncargado', nombre: 'Nombre del encargado', ordenar: true },
    { campo: 'direccionEmpresa', nombre: 'Direcion', ordenar: true },
    { campo: 'nitEmpresa', nombre: 'Nit de la empresa', ordenar: true },
    { campo: 'evento', nombre: 'Evento', ordenar: true },
    { campo: 'estado', nombre: 'Estado', ordenar: true },
    { campo: 'acciones', nombre: 'Acciones' },
  ])

  const contenidoTabla: Array<Array<ReactNode>> = parametrosData.map(
    (parametroData, indexParametro) => [
      <Typography
        key={`${parametroData.id}-${indexParametro}-codigo`}
        variant={'body2'}
      >{`${parametroData.nombreEmpresa}`}</Typography>,
      <Typography
        key={`${parametroData.id}-${indexParametro}-nombre`}
        variant={'body2'}
      >
        {`${parametroData.rubroEmpresa}`}
      </Typography>,
      <Typography
        key={`${parametroData.id}-${indexParametro}-descripcion`}
        variant={'body2'}
      >{`${parametroData.telefonoEmpresa}`}</Typography>,
      <Typography
        key={`${parametroData.id}-${indexParametro}-grupo`}
        variant={'body2'}
      >{`${parametroData.correoEmpresa}`}</Typography>,
      <Typography
        key={`${parametroData.id}-${indexParametro}-grupo`}
        variant={'body2'}
      >{`${parametroData.descripcionEmpresa}`}</Typography>,
      <Typography
        key={`${parametroData.id}-${indexParametro}-grupo`}
        variant={'body2'}
      >{`${parametroData.nombreEncargado}`}</Typography>,
      <Typography
        key={`${parametroData.id}-${indexParametro}-grupo`}
        variant={'body2'}
      >{`${parametroData.direccionEmpresa}`}</Typography>,
      <Typography
        key={`${parametroData.id}-${indexParametro}-grupo`}
        variant={'body2'}
      >{`${parametroData.nitEmpresa}`}</Typography>,
      <Typography
        key={`${parametroData.id}-${indexParametro}-grupo`}
        variant={'body2'}
      >{`${parametroData.evento}`}</Typography>,
      <CustomMensajeEstado
        key={`${parametroData.id}-${indexParametro}-estado`}
        titulo={parametroData.estado}
        descripcion={parametroData.estado}
        color={
          parametroData.estado == 'ACTIVO'
            ? 'success'
            : parametroData.estado == 'INACTIVO'
              ? 'error'
              : 'info'
        }
      />,

      <Grid key={`${parametroData.id}-${indexParametro}-acciones`}>
        {permisos.update && (
          <IconoTooltip
            id={`cambiarEstadoParametro-${parametroData.id}`}
            titulo={parametroData.estado == 'ACTIVO' ? 'Inactivar' : 'Activar'}
            color={parametroData.estado == 'ACTIVO' ? 'success' : 'error'}
            accion={async () => {
              await editarEstadoParametroModal(parametroData)
            }}
            desactivado={parametroData.estado == 'PENDIENTE'}
            icono={
              parametroData.estado == 'ACTIVO' ? 'toggle_on' : 'toggle_off'
            }
            name={
              parametroData.estado == 'ACTIVO'
                ? 'Inactivar Parámetro'
                : 'Activar Parámetro'
            }
          />
        )}

        {permisos.update && (
          <IconoTooltip
            id={`editarParametros-${parametroData.id}`}
            name={'Parámetros'}
            titulo={'Editar'}
            color={'primary'}
            accion={() => {
              imprimir(`Editaremos`, parametroData)
              editarParametroModal(parametroData)
            }}
            icono={'edit'}
          />
        )}
      </Grid>,
    ]
  )

  const acciones: Array<ReactNode> = [
    <Button
    key="descargarPdf"
    sx={{ marginLeft: '5px', borderRadius: 5 }}
    startIcon={<Icono color="inherit">get_app</Icono>}
    onClick={() => {
      generarPDF()
    }}
    variant="contained"
    color="primary"
  >
    Descargar PDF
  </Button>,
    <BotonBuscar
      id={'accionFiltrarParametrosToggle'}
      key={'accionFiltrarParametrosToggle'}
      seleccionado={mostrarFiltroParametros}
      cambiar={setMostrarFiltroParametros}
    />,
    xs && (
      <BotonOrdenar
        id={'ordenarParametros'}
        key={`ordenarParametros`}
        label={'Ordenar parámetros'}
        criterios={ordenCriterios}
        cambioCriterios={setOrdenCriterios}
      />
    ),
    <IconoTooltip
      id={'actualizarParametro'}
      titulo={'Actualizar'}
      key={`accionActualizarParametro`}
      accion={async () => {
        await obtenerParametrosPeticion()
      }}
      icono={'refresh'}
      name={'Actualizar lista de parámetros'}
    />,
    permisos.create && (
      <IconoBoton
        id={'agregarParametro'}
        key={'agregarParametro'}
        texto={'Agregar'}
        variante={xs ? 'icono' : 'boton'}
        icono={'add_circle_outline'}
        descripcion={'Agregar parámetro'}
        accion={() => {
          agregarParametroModal()
        }}
      />
    ),
  ]

  const obtenerParametrosPeticion = async () => {
    try {
      setLoading(true)

      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/empresas`,
        params: {
          pagina: pagina,
          limite: limite,
          ...(filtroParametro.length == 0 ? {} : { filtro: filtroParametro }),
          ...(ordenFiltrado(ordenCriterios).length == 0
            ? {}
            : {
                orden: ordenFiltrado(ordenCriterios).join(','),
              }),
        },
      })
      setParametrosData(respuesta.datos?.filas)
      setTotal(respuesta.datos?.total)
      setErrorParametrosData(null)
    } catch (e) {
      imprimir(`Error al obtener parametros`, e)
      setErrorParametrosData(e)
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: 'error' })
    } finally {
      setLoading(false)
    }
  }

  const agregarParametroModal = () => {
    setParametroEdicion(undefined)
    setModalParametro(true)
  }
  const editarParametroModal = (parametro: EmpresaCrudType) => {
    setParametroEdicion(parametro)
    setModalParametro(true)
  }

  const cerrarModalParametro = async () => {
    setModalParametro(false)
    await delay(500)
    setParametroEdicion(undefined)
  }

  async function definirPermisos() {
    setPermisos(await permisoUsuario(pathname))
  }

  useEffect(() => {
    definirPermisos().finally()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    obtenerParametrosPeticion().finally(() => {})
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    pagina,
    limite,
    // eslint-disable-next-line react-hooks/exhaustive-deps
    JSON.stringify(ordenCriterios),
    filtroParametro,
  ])

  useEffect(() => {
    if (!mostrarFiltroParametros) {
      setFiltroParametro('')
    }
  }, [mostrarFiltroParametros])

  const paginacion = (
    <Paginacion
      pagina={pagina}
      limite={limite}
      total={total}
      cambioPagina={setPagina}
      cambioLimite={setLimite}
    />
  )

  return (
    <>
      <title>{`Empresas - ${siteName()}`}</title>
      <AlertDialog
        isOpen={mostrarAlertaEstadoParametro}
        titulo={'Alerta'}
        texto={`¿Está seguro de ${
          parametroEdicion?.estado == 'ACTIVO' ? 'inactivar' : 'activar'
        } la empresa: ${titleCase(parametroEdicion?.nombreEmpresa ?? '')} ?`}
      >
        <Button onClick={cancelarAlertaEstadoParametro}>Cancelar</Button>
        <Button onClick={aceptarAlertaEstadoParametro}>Aceptar</Button>
      </AlertDialog>
      <CustomDialog
        isOpen={modalParametro}
        handleClose={cerrarModalParametro}
        title={parametroEdicion ? 'Editar empresa' : 'Nuevo empresa'}
      >
        <VistaModalEmpresas
          parametro={parametroEdicion}
          accionCorrecta={() => {
            cerrarModalParametro().finally()
            obtenerParametrosPeticion().finally()
          }}
          accionCancelar={cerrarModalParametro}
        />
      </CustomDialog>
      <CustomDataTable
        titulo={'Empresas'}
        error={!!errorParametrosData}
        cargando={loading}
        acciones={acciones}
        columnas={ordenCriterios}
        cambioOrdenCriterios={setOrdenCriterios}
        paginacion={paginacion}
        contenidoTabla={contenidoTabla}
        filtros={
          mostrarFiltroParametros && (
            <FiltroEmpresa
              filtroParametro={filtroParametro}
              accionCorrecta={(filtros) => {
                setPagina(1)
                setLimite(10)
                setFiltroParametro(filtros.parametro)
              }}
              accionCerrar={() => {
                imprimir(`👀 cerrar`)
              }}
            />
          )
        }
      />
    </>
  )
}
